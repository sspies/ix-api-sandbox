"""
JEA URL Configuration
"""
from django.conf.urls import url
from django.urls import include

from jea import admin
from jea.public import urls as public_urls
from jea.api.v1 import urls as api_v1_urls
from jea.api.docs import urls as api_docs_urls

urlpatterns = [
    url(r"^", include(public_urls)),
    url(r"^admin/", admin.site.urls),
    url(r"^api/v1/", include(api_v1_urls)),
    url(r"^api/docs/", include(api_docs_urls)),
]

