
"""
Text utils
"""


def trim_docstring(docstring):
    """
    Leftalign docstring.
    This is taken from: PEP 257
    """
    if not docstring:
        return ""

    # Arbitrary chosen by unfair dice role
    max_indent = 100000

    # Convert tabs to spaces (following the normal Python rules)
    # and split into a list of lines:
    lines = docstring.expandtabs().splitlines()
    # Determine minimum indentation (first line doesn't count):
    indent = max_indent
    for line in lines[1:]:
        stripped = line.lstrip()
        if stripped:
            indent = min(indent, len(line) - len(stripped))
    # Remove indentation (first line is special):
    trimmed = [lines[0].strip()]
    if indent < max_indent:
        for line in lines[1:]:
            trimmed.append(line[indent:].rstrip())
    # Strip off trailing and leading blank lines:
    while trimmed and not trimmed[-1]:
        trimmed.pop()
    while trimmed and not trimmed[0]:
        trimmed.pop(0)
    # Return a single string:
    return '\n'.join(trimmed)


def description_text(text):
    """Extract description, removing inlines like 'example'"""
    lines = text.split("\n")
    text = "\n".join(
        l for l in lines
        if not l.lower().startswith("example"))

    return text


def example_text(text):
    """Extract example only"""
    lines = text.split("\n")
    example = ""
    for line in lines:
        if line.lower().startswith("example"):
            example = line

    # Remove hint
    tokens = example.split(":", 1)
    if len(tokens) == 2:
        example = tokens[1]

    return example.strip()


