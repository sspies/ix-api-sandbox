

"""
Test custom filters.

Caveat: this is using a jea model, which is a bit of a bad
design. Yet I don't want to have this piece of code untested.

"""

import pytest
import django_filters
from model_mommy import mommy


from jea.catalog import models
from utils import filters


@pytest.mark.django_db
def test_bulk_id_filter():
    """Test bulk id filtering"""

    # Make some models
    p1 = mommy.make("catalog.Product")
    p2 = mommy.make("catalog.Product")
    p3 = mommy.make("catalog.Product")

    bulk_filter = filters.BulkIdFilter()

    # Test single value
    qs = models.Product.objects.all()
    qs = bulk_filter.filter(qs, str(p1.pk))

    assert qs.count() == 1
    assert qs.first() == p1

    # Test multivalue
    pks = f"{p3.pk},{p2.pk}"
    query_set = models.Product.objects.all()
    query_set = bulk_filter.filter(query_set, pks)

    assert query_set.count() == 2
    assert not p1 in query_set
    assert p2 in query_set
    assert p3 in query_set



@pytest.mark.django_db
def test_bulk_id_filterset():
    """Test bulk filter in FilterSet context"""
    class FooFilter(django_filters.FilterSet):
        id = filters.BulkIdFilter()

        class Meta:
            model = models.Product
            exclude = []

    # Make some models
    p1 = mommy.make("catalog.Product")
    p2 = mommy.make("catalog.Product")
    p3 = mommy.make("catalog.Product")

    filtered = FooFilter({
        "id": f"{p3.pk},{p2.pk}",
    })
    query_set = filtered.qs

    assert query_set.count() == 2
    assert not p1 in query_set
    assert p2 in query_set
    assert p3 in query_set


def test_must_list__single_value():
    """Test assert list from input: single value"""
    value = "foo"
    assert ["foo"] == filters.must_list(value)

    value = 42
    assert [42] == filters.must_list(value)

    value = None
    assert [None] == filters.must_list(value)


def test_must_list__list_value():
    """Test assert list from input: list"""
    value = ["foo", 42]
    assert value == filters.must_list(value)


def test_must_list__csv():
    """Test assert list from input: CSV"""
    csv = "23,42,5"
    result = filters.must_list(csv)
    assert result == ["23", "42", "5"]

