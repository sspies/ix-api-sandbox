
"""
Tests for datastructure utilities
"""

from utils import datastructures


def test_reverse_mapping():
    """Test reverse mapping"""
    mapping = {
        "foo": 23,
        "bar": "baz",
    }

    inverse = datastructures.reverse_mapping(mapping)

    assert inverse[23] == "foo"
    assert inverse["baz"] == "bar"


def test_whitelist_filter_fields():
    """Test dict key whitelisting"""
    data = {
        "foo": 23,
        "bar": 42,
        "baz": {
            "bam": 12,
        }
    }

    filtered = datastructures.whitelist_filter_fields(data, [
        "foo", "baz"
    ])

    assert not id(filtered) == id(data), "filtered should be a copy of data"
    assert data["bar"], "The original data should not be touched."
    assert not filtered.get("bar")
    assert not id(filtered["baz"]) == id(data["baz"])

