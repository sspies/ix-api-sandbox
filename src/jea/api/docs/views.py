
"""
Api Documentations Views
"""

from django.shortcuts import render

def serve_redoc(request):
    """Render redoc api documentation viewer"""
    return render(request, "docs/redoc.html")
