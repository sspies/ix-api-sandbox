
"""
Get documentation from problem classes.
Find problem classes given a path.
"""

import importlib
import pkgutil
import inspect


def find_problems(api_base):
    """
    Find problemsgiven a api base
    """
    packages = [p.name for p in pkgutil.walk_packages(api_base.__path__)
                if p.ispkg]

    # Convetion: <base>.<package>.problems
    problem_classes = set()
    for package in packages:
        module_name = f"{api_base.__name__}.{package}.problems"
        try:
            module = importlib.import_module(module_name)
        except ImportError:
            continue

        module_problems = inspect.getmembers(module, inspect.isclass)
        problem_classes = problem_classes.union(c for (_, c) in module_problems)

    # Sort classes
    problem_classes = sorted(
        problem_classes, key=lambda c: c.__name__)

    return problem_classes


