
"""
OpenAPI Schema Generator

Subclass the rest_framework OpenAPI schema generator,
and use a custom schema rendering for the serializer
schemata.
"""

# Caveat: This is a bit hacky. I'd rather prefer a more
#         elegant solution - either with an already working
#         standalone library like yasg or with rest_framework
#         on board tools like the coreapi schema generators.
#
#         However, the former does not support OpenAPI 3
#         and the latter has issues with getting a correct
#         serializer schema. I tried. I failed. I'm rolling
#         my own.
#

import collections

from rest_framework import schemas, renderers
from rest_framework.schemas import generators
from django.utils.six.moves.urllib import parse as urlparse

from backend import settings
from utils import text as text_utils
from jea.api.v1.errors import serializers as error_serializers
from jea.api.docs.openapi.generators import (
    serializers_meta,
    filters_meta,
)
from jea.api.docs.openapi.reflection import (
    serializers as serializers_reflection,
)


#
# Helper
#
def _get_auth_description():
    """
    Get the description on how auth works.
    NOTE: This is a temporary workaround!
    """
    from jea.api.v1.auth import views as auth_views
    return text_utils.trim_docstring(auth_views.TokenViewSet.__doc__)


class Generator(schemas.SchemaGenerator):
    """
    Override the get_links method and inject the
    serializer reference name in the view's link.

    This feels like kind of a hack, so why are we doing
    this?
    We need more control on how the serializer schemas
    are rendered, but since the schema is genreated directly
    without the coreapi layer, we perform the serializer
    schema rendering in the schema renderer directly from
    the serializer class and reference the result.
    """
    def get_links(self, request=None):
        """
        Return a dictionary containing all the links that should be
        included in the API schema.
        """
        links = generators.LinkNode()

        # Generate (path, method, view) given (path, method, callback).
        paths = []
        view_endpoints = []
        for path, method, callback in self.endpoints:
            view = self.create_view(callback, method, request)
            path = self.coerce_path(path, method, view)
            paths.append(path)
            view_endpoints.append((path, method, view))

        # Only generate the path prefix for paths that will be included
        if not paths:
            return None
        prefix = self.determine_path_prefix(paths)

        for path, method, view in view_endpoints:
            # if not self.has_view_permissions(path, method, view):
            #    continue
            link = view.schema.get_link(path, method, base_url=self.url)
            subpath = path[len(prefix):]
            keys = self.get_keys(subpath, method, view)
            action = keys[-1]

            if not self.has_response_body(view, action):
                continue

            response_schemas = self.get_response_schemas(view, action)
            if not response_schemas:
                continue
            # Check if the method is present in the response schema
            # otherwise, hide this from documentation


            # Add hints to link
            link.__response_schemas__ = response_schemas
            link.__request_schema__ = self.get_request_schema(view, action)
            link.__tag_description__ = text_utils.trim_docstring(view.__doc__)
            if action == "list":
                link.__list_filter_parameters__ = \
                    self.get_list_filter_parameters(view)

            generators.insert_into(links, keys, link)

        return links

    def has_response_body(self, view, action):
        """Check if the schema is defined for a given action"""
        responses = getattr(view, "responses", None)
        if not responses:
            return False
        response_bodies = responses.get(action)
        if not response_bodies:
            return False
        return True

    def get_list_filter_parameters(self, view):
        """Get filters for 'list' requests"""
        filterset_class = getattr(view, "filterset_class", None)
        if not filterset_class:
            return []

        return [filters_meta.frag_from_filter(name, query_filter)
                for name, query_filter 
                in filterset_class.get_filters().items()]

    def get_request_schema(self, view, action):
        """
        Get the request body schema if there is any.
        """
        responses = getattr(view, "responses", None)
        if not responses:
            return None
        response_bodies = responses.get(action)
        if not response_bodies:
            return None

        request_serializer = response_bodies.get("request")
        if not request_serializer:
            return None

        return self.get_response_schema(request_serializer, None)

    def get_response_schemas(self, view, action):
        """
        Get the response mappings, if there are any in the
        view and create response schemas from it.
        """
        responses = getattr(view, "responses", None)
        if not responses:
            return None
        response_bodies = responses.get(action)
        if not response_bodies:
            return None

        response_schemas = {
            str(status_code): self.get_response_schema(
                serializer,
                self.get_response_description(view, action, status_code))
            for status_code, serializer in response_bodies.items()
            if status_code not in ["request", "problems"]
        }

        # Add problem schemas
        # Collect status codes from problems
        problems = response_bodies.get("problems", [])
        # Group by response status code
        problem_responses = collections.defaultdict(list)
        for prob in problems:
            serializer = error_serializers.ProblemSerializer(prob)
            problem_responses[prob.response_status].append(serializer)

        for status_code, serializers in problem_responses.items():
            response_schemas[str(status_code)] = \
                self.get_problem_response_schema(serializers)

        return response_schemas


    def get_problem_response_schema(self, serializers):
        """
        Get the error schema based on the serializers types
        and status code.
        """
        problem_types = [ser.data["type"] for ser in serializers]
        status = [ser.instance.response_status for ser in serializers][0]
        description = [ser.instance.__class__.__name__ for ser in serializers][0]
        description = description.replace("Problem", "")

        schema = {
            "description": description,
            "content": {
                "application/json": {
                    "schema": {
                        "type": "object",
                        "allOf": [
                            {"$ref": "#/components/schemas/problem"},
                            {
                                "type": "object",
                                "properties": {
                                    "type": {
                                        "type": "string",
                                        "enum": problem_types,
                                    },
                                    "title": {
                                        "example":
                                            serializers[0].data["title"],
                                    },
                                    "status": {
                                        "example": status,
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        }

        return schema


    def get_response_schema(self, serializer, description):
        """
        Based on the  tag and the serializer,
        create a response schema.

        Why here and not in the renderer where it belongs?
        Because we did not generalized this and generate
        the schema directly from the serializer without
        an intermediat coreapi / coreschema datastructure.
        """
        # I'm sorry.
        if hasattr(serializer, "child"):
            serializer_class = type(serializer.child)
        else:
            serializer_class = type(serializer)

        serializer_ref = serializers_meta.serializer_ref_name(
            serializer_class, serializer.partial)

        schema = {
            "$ref": "#/components/schemas/{}".format(serializer_ref),
        }

        if getattr(serializer, "many", False):
            # Wrap in array response
            schema = {
                "type": "array",
                "items": schema,
            }

        # Assert that description is set
        if not description:
            description = ""

        return {
            "description": description,
            "content": {
                "application/json": {
                    "schema": schema,
                }
            }
        }

    def get_response_description(self, view, action, status_code):
        """Get the description of a serializer"""
        responses = getattr(view, "responses", None)
        if not responses:
            return None
        response_bodies = responses.get(action)
        if not response_bodies:
            return None
        response_serializer = response_bodies.get(status_code)
        if not response_serializer:
            return None

        return text_utils.trim_docstring(response_serializer.__doc__)


class Renderer(renderers.OpenAPIRenderer):
    """
    Override `get_structure` to inject global
    `components.schemas` where all project related
    serializer schemas reside. These can be referenced
    in response body.

    The `get_operation` method is overriden to achive this.
    """
    def __init__(self, api_base):
        """Initialize OpenAPI renderer"""
        super().__init__()
        self.api_base = api_base

    def get_parameters(self, link):
        """Overridden get parameters to enforce a description"""
        parameters = []
        for field in link.fields:
            if field.location not in ['path', 'query']:
                continue
            parameter = {
                "name": field.name,
                "in": field.location,
                "description": f"Get by {field.name}",
            }
            if field.required:
                parameter['required'] = True
            if field.description:
                parameter['description'] = field.description
            if field.schema:
                parameter['schema'] = self.get_schema(field.schema)
            parameters.append(parameter)
        return parameters

    def get_operation(self, link, name, tag):
        """Create the schema for an operation"""
        operation_id = "%s_%s" % (tag, name) if tag else name
        parameters = self.get_parameters(link)
        if not parameters:
            parameters = []
        list_filter_parameters = getattr(
            link, "__list_filter_parameters__", None)
        if list_filter_parameters:
            parameters = parameters + list_filter_parameters

        operation = {
            "operationId": operation_id,
        }

        if link.title:
            operation["summary"] = link.title
        if link.description:
            operation["description"] = link.description
        if parameters:
            operation["parameters"] = parameters
        if tag:
            operation["tags"] = [tag]

        responses = getattr(link, "__response_schemas__", None)
        if responses:
            operation["responses"] = responses

        request_body = getattr(link, "__request_schema__", None)
        if request_body:
            operation["requestBody"] = request_body

        return operation

    def get_serializers_schemas(self):
        """Get schemas from serializers in `api_base` module"""
        return serializers_reflection.get_schemas(self.api_base)

    def get_paths(self, document):
        paths = {}

        tag = None
        for name, link in document.links.items():
            path = urlparse.urlparse(link.url).path
            method = link.action.lower()
            paths.setdefault(path, {})
            paths[path][method] = self.get_operation(link, name, tag=tag)

        for tag, section in document.data.items():
            for name, link in section.links.items():
                path = urlparse.urlparse(link.url).path
                method = link.action.lower()
                paths.setdefault(path, {})
                paths[path][method] = self.get_operation(link, name, tag=tag)

            if not section.data:
                continue

            for tag_, section_ in section.data.items():
                for name, link in section_.links.items():
                    path = urlparse.urlparse(link.url).path
                    method = link.action.lower()
                    paths.setdefault(path, {})
                    paths[path][method] = self.get_operation(
                        link, tag_, tag=tag)

        return paths

    def get_tags(self, document):
        """Get all tags and descriptions from document"""
        tags = [{
            "name": "auth",
            "description": _get_auth_description(),
        }]
        # Include all tags
        for tag, section in document.data.items():
            # Get first endpoint and get the views info
            endpoints = list(section.links.items())
            if not endpoints:
                continue

            tags.append({
                "name": tag,
                "description": endpoints[0][1].__tag_description__,
            })

        tags = sorted(tags, key=lambda t: t['name'])

        return tags


    def get_structure(self, data):
        """Build the OpenAPI 3 document"""
        self.get_tags(data)
        return {
            "openapi": "3.0.0",
            "info": {
                "version": settings.VERSION,
                "title": "IX-API",
                "description": (
                    "This API allows to config/change/delete "
                    "Internet Exchange services"),
                "contact": {
                    "url": "https://ix-api.net",
                    "email": "team@ix-api.net",
                }
            },
            "servers": [{
                "url": "/api/v1", # data.url
            }],
            "paths": self.get_paths(data),
            "components": {
                "schemas": self.get_serializers_schemas(),
            },
            "tags": self.get_tags(data),
        }
