"""
Extract query parameters from filterset
"""
import enum

from django_filters import filters

from utils import (
    filters as extra_filters,
    text as extra_text_utils,
)


def frag_from_filter(name, field):
    """Get filter field schema fragment"""
    frag = {
        "name": name,
        "schema": _schema_from_filter(field),
        "required": False, # We only have optional filters
        "in": "query",
        "description": f"Filter by {name}",
    }

    # Extract description and example
    if field.label:
        label = extra_text_utils.trim_docstring(field.label)
        frag["description"] = extra_text_utils.description_text(label)

        # Extract example from docstring
        example_text = extra_text_utils.example_text(label)
        try:
            example = json.loads(example_text)
        except:
            example = example_text

        if example:
            frag["example"] = example

    if isinstance(field, extra_filters.BulkIdFilter):
        frag["style"] = "form"
        frag["explode"] = False

    return frag


def _schema_from_filter(field):
    """Get schema from filter field"""
    if isinstance(field, filters.NumberFilter):
        return {
            "type": "integer",
        }
    if isinstance(field, filters.DateTimeFilter):
        return {
            "type": "string",
            "format": "date-time",
        }
    if isinstance(field, filters.CharFilter):
        return {
            "type": "string",
        }
    if isinstance(field, filters.ModelMultipleChoiceFilter):
        return {
            "type": "string",
            "example": "id1,id2",
        }
    if isinstance(field, filters.ModelChoiceFilter):
        return {
            "type": "string",
        }
    if isinstance(field, filters.ChoiceFilter):
        choices = field.extra['choices']
        return {
            "type": _choices_type(choices),
            "enum": _choices_enum(choices),
        }
    if isinstance(field, extra_filters.BulkIdFilter):
        return {
            "type": "array",
            "items": {
                "type": "string",
            },
            "example": "id1,id2,id3",
        }

    raise NotImplementedError(
        "Schema not implemented for filter type: {}".format(
            type(field)))


def _choices_type(choices):
    """Derive the choices type from the enum value"""
    value = choices[0][0]
    if isinstance(value, enum.Enum):
        value = value.value

    if isinstance(value, str):
        return "string"

    return "integer"


def _choices_enum(choices):
    """Get choices values"""
    return [_unpack_choice_value(c) for c in choices]


def _unpack_choice_value(value):
    """Unpack value"""
    if isinstance(value, tuple):
        value = value[0]
    if isinstance(value, enum.Enum):
        return value.value

    return value
