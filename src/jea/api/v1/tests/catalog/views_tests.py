
"""
Test Catalog Views
"""

import pytest
from model_mommy import mommy
from rest_framework import test

from jea.api.v1.tests import authorized_requests
from jea.api.v1.catalog import views
from jea.api.v1.auth import users


@pytest.mark.django_db
def test_facilities_view_set__list():
    """Test list all facilities"""
    customer = mommy.make("crm.Customer")
    view = views.FacilitiesViewSet.as_view({"get": "list"})

    # Make request
    request = authorized_requests.get(customer)
    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_facilities_view_set__retrieve():
    """Test retrieving a facility"""
    customer = mommy.make("crm.Customer")
    facility = mommy.make("catalog.Facility")

    # Make request
    view = views.FacilitiesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(customer)
    response = view(request, pk=facility.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_devices_view_set__list():
    """Test listing devices"""
    customer = mommy.make("crm.Customer")
    view = views.DevicesViewSet.as_view({"get": "list"})
    request = authorized_requests.get(customer)

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_devices_view_set__retrieve():
    """Test retrieving a device"""
    customer = mommy.make("crm.Customer")
    device = mommy.make("catalog.Device")

    view = views.DevicesViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(customer)

    response = view(request, pk=device.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_points_of_presence_view_set__list():
    """Test getting all pops"""
    customer = mommy.make("crm.Customer")
    request = authorized_requests.get(customer)
    view = views.PointsOfPresenceViewSet.as_view({
        "get": "list",
    })

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_points_of_presence_view_set__retrieve():
    """Test getting a specific device"""
    customer = mommy.make("crm.Customer")
    pop = mommy.make("catalog.PointOfPresence")
    request = authorized_requests.get(customer)
    view = views.PointsOfPresenceViewSet.as_view({
        "get": "retrieve",
    })

    response = view(request, pk=pop.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_products_view_set__list():
    """Test getting all the products"""
    customer = mommy.make("crm.Customer")
    request = authorized_requests.get(customer)
    view = views.ProductsViewSet.as_view({
        "get": "list",
    })

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_products_view_set__retrieve():
    """Test getting a specific product"""
    customer = mommy.make("crm.Customer")
    product = mommy.make("catalog.ExchangeLanNetworkProduct")
    request = authorized_requests.get(customer)
    view = views.ProductsViewSet.as_view({"get": "retrieve"})

    response = view(request, pk=product.pk)
    assert response.status_code == 200


