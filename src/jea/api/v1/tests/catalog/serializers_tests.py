
"""
Test Catalog Serializers
"""

import pytest
from model_mommy import mommy

from jea.catalog.models import (
    PointOfPresence,
    Device, DeviceCapability,
    Facility, FacilityCluster, FacilityOperator,
    ExchangeLanNetworkProduct,
    ELineNetworkProduct,
    CloudNetworkProduct,
    ClosedUserGroupNetworkProduct,

    PRODUCT_TYPE_EXCHANGE_LAN,
    PRODUCT_TYPE_CLOSED_USER_GROUP,
    PRODUCT_TYPE_ELINE,
    PRODUCT_TYPE_CLOUD,
)
from jea.api.v1.catalog.serializers import (
    FacilitySerializer,
    PointOfPresenceSerializer,
    DeviceSerializer,
    DeviceCapabilitySerializer,
    ProductSerializer,
)

def test_facility_serializer():
    """Test facility serialization"""
    cluster = mommy.prepare(FacilityCluster)
    operator = mommy.prepare(FacilityOperator)
    facility = mommy.prepare(Facility, cluster=cluster, operator=operator)

    serializer = FacilitySerializer(facility)

    result = serializer.data
    assert result, "Serializer should have generated a result"

    assert result["cluster"] == cluster.name
    assert result["organisation_name"] == operator.name


def test_devices_serializer():
    """Test device serialization"""
    device = mommy.prepare(Device)
    serializer = DeviceSerializer(device)

    result = serializer.data
    assert result, "The serializer should produce data"


@pytest.mark.django_db
def test_devices_serializer_capabilities():
    """Test embedded capabilities"""
    capa_1 = mommy.make(DeviceCapability)
    capa_2 = mommy.make(DeviceCapability)
    capa_3 = mommy.make(DeviceCapability)

    device = mommy.make(Device, capabilities=[capa_1, capa_2, capa_3])

    serializer = DeviceSerializer(device)

    result = serializer.data
    assert result, "The serializer should produce data"
    assert result["capabilities"]
    assert len(result["capabilities"]) == 3


def test_device_capability_serializer():
    """Test the serialization of a device capability"""
    capa = mommy.prepare(DeviceCapability)
    serializer = DeviceCapabilitySerializer(capa)

    assert serializer.data, "Serializer should produce data"


@pytest.mark.django_db
def test_point_of_presence_serializer():
    """Test serialization of a pop"""
    device_a = mommy.make(Device, name="device_a")
    device_b = mommy.make(Device, name="device_b")

    capa = mommy.make(DeviceCapability, device=device_a)

    facility = mommy.make(Facility)
    remote_facility = mommy.make(Facility)

    pop_a = mommy.make(PointOfPresence,
                       physical_devices=[device_a],
                       reachable_devices=[device_b],
                       physical_facility=facility)

    pop_b = mommy.make(PointOfPresence,
                       physical_devices=[device_b],
                       physical_facility=remote_facility)

    serializer = PointOfPresenceSerializer(pop_a)
    result = serializer.data
    assert result, "Serializer should produce data without crashing."

#
# Polymorphic Product Serialization
#
def test_polymorphic_product_serialization():
    """Test polymorphic product serializer"""
    exchange_lan_product = mommy.prepare(ExchangeLanNetworkProduct)
    eline_product = mommy.prepare(ELineNetworkProduct)
    closed_user_group_product = mommy.prepare(ClosedUserGroupNetworkProduct)
    cloud_product = mommy.prepare(CloudNetworkProduct)

    # Serialize products
    result = ProductSerializer(exchange_lan_product).data
    assert result["type"] == PRODUCT_TYPE_EXCHANGE_LAN

    """
    result = ProductSerializer(eline_product).data
    assert result["type"] == PRODUCT_TYPE_ELINE

    result = ProductSerializer(closed_user_group_product).data
    assert result["type"] == PRODUCT_TYPE_CLOSED_USER_GROUP

    result = ProductSerializer(cloud_product).data
    assert result["type"] == PRODUCT_TYPE_CLOUD
    """
