
from rest_framework import (
    serializers,
    exceptions as rf_exceptions,
)

from jea.access import exceptions as access_exceptions
from jea.api.v1.access import serializers as access_serializers

from jea.api.v1.errors import (
    problems,
    problem_factory,
)


def test__make_validation_problem():
    """Test making a validation problem out of an error"""
    err = access_exceptions.DemarcationPointUnavailable()
    problem = problem_factory._make_validation_problem(err)

    assert problem.extra["properties"]
    assert problem.extra["properties"][0]["name"]

    print(problem.__dict__)

def test__make_validation_problem_serializer_error():
    """Test making a problem out of a serializer error"""
    problem = None # Uh oh. We don't have a problem!

    # Create simple serializer
    class Foo(serializers.Serializer):
        field = serializers.CharField()

    serializer = Foo(data={})
    try:
        serializer.is_valid(raise_exception=True)
    except Exception as e:
        problem = problem_factory._make_validation_problem(e)

    assert problem
    assert problem.extra["properties"]
    assert problem.extra["properties"][0]["name"]

    print(problem.__dict__)


def test__make_problem_from_api_exception():
    """
    Test rest framework exceptions
    """
    expected = [
        (rf_exceptions.ValidationError(),
         problems.ValidationErrorProblem),
        (rf_exceptions.ParseError(),
         problems.ParseErrorProblem),
        (rf_exceptions.AuthenticationFailed(),
         problems.AuthenticationProblem),
        (rf_exceptions.NotAuthenticated(),
         problems.NotAuthenticatedProblem),
        (rf_exceptions.PermissionDenied(),
         problems.PermissionDeniedProblem),
        (rf_exceptions.NotFound(),
         problems.NotFoundProblem),
        (rf_exceptions.MethodNotAllowed("FNRD"),
         problems.MethodNotAllowedProblem),
        (rf_exceptions.NotAcceptable(),
         problems.NotAcceptableProblem),
        (rf_exceptions.Throttled(),
         problems.ServerErrorProblem),
    ]

    for exc, problem_class in expected:
        problem = problem_factory._make_problem_from_api_exception(exc)
        assert isinstance(problem, problem_class)

