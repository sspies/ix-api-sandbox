
"""
Test Service Serializers
"""

import pytest
from model_mommy import mommy

from jea.api.v1.service.serializers import (
    NetworkServiceBaseSerializer,
    ExchangeLanNetworkServiceSerializer,
    ClosedUserGroupNetworkServiceSerializer,
    ELineNetworkServiceSerializer,
    CloudNetworkServiceSerializer,
    NetworkServiceSerializer,
    IXPSpecificFeatureFlagSerializer,
    NetworkFeatureBaseSerializer,
    BlackholingNetworkFeatureSerializer,
    RouteServerNetworkFeatureSerializer,
    IXPRouterNetworkFeatureSerializer,
    NetworkFeatureSerializer,
)

#
# Helper: Create a service with product
#
def _make_service(service_model, product_model):
    """Create a service with a product"""
    product = mommy.make(product_model)
    network_service = mommy.make(
        service_model,
        product=product,
        required_contact_types=["noc"])

    return network_service


@pytest.mark.django_db
def test_network_service_base_serializer():
    """
    Test serializer base with NetworkService object
    """
    network_service = _make_service(
        "service.NetworkService",
        "catalog.ExchangeLanNetworkProduct",
    )
    serializer = NetworkServiceBaseSerializer(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"

    # Check references
    assert isinstance(result["product"], str)
    assert isinstance(result["managing_customer"], str)
    assert isinstance(result["consuming_customer"], str)

    assert isinstance(result["network_features"], list)
    assert isinstance(result["required_contact_types"], list)
    assert "noc" in result["required_contact_types"]


@pytest.mark.django_db
def test_exchange_lan_network_serializer():
    """
    Test serialization of exchange lan network services
    """
    network_service = _make_service(
        "service.ExchangeLanNetworkService",
        "catalog.ExchangeLanNetworkProduct",
    )
    serializer = ExchangeLanNetworkServiceSerializer(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"

    # Check embedded / inlines
    assert isinstance(result["ips"], list)


@pytest.mark.django_db
def test_closed_user_group_serializer():
    """
    Test serialization of closed user groups
    """
    network_service = _make_service(
        "service.ClosedUserGroupNetworkService",
        "catalog.ClosedUserGroupNetworkProduct",
    )
    serializer = ClosedUserGroupNetworkServiceSerializer(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_eline_serializer():
    """
    Test serialization of eline network services
    """
    network_service = _make_service(
        "service.ELineNetworkService",
        "catalog.ELineNetworkProduct",
    )
    serializer = ELineNetworkServiceSerializer(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_cloud_network_service_serializer():
    """
    Test serialization of cloud network services
    """
    network_service = _make_service(
        "service.CloudNetworkService",
        "catalog.CloudNetworkProduct",
    )
    serializer = ClosedUserGroupNetworkServiceSerializer(network_service)

    result = serializer.data
    assert result, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_polymorphic_network_service_serializer_serialize():
    """
    Test the polymorphic network service serializer
    serialization method.
    """
    exchange_lan = _make_service(
        "service.ExchangeLanNetworkService",
        "catalog.ExchangeLanNetworkProduct",
    )
    closed_user_group = _make_service(
        "service.ClosedUserGroupNetworkService",
        "catalog.ClosedUserGroupNetworkProduct",
    )
    eline = _make_service(
        "service.ELineNetworkService",
        "catalog.ELineNetworkProduct",
    )
    cloud = _make_service(
        "service.CloudNetworkService",
        "catalog.CloudNetworkProduct",
    )

    services = [exchange_lan, closed_user_group, eline, cloud]
    serializer = NetworkServiceSerializer(services, many=True)
    result = serializer.data

    assert result, "Serializer should serialize without crashing."

#
# Features
#
def test_ixp_specific_feature_flag_serializer():
    """Test feature flag serialization"""
    feature_flag = mommy.prepare("service.IXPSpecificFeatureFlag")
    serializer = IXPSpecificFeatureFlagSerializer(feature_flag)
    assert serializer.data, "Serializer should serialize without crashing"


def test_feature_base_serializer():
    """Test base serializer for features"""
    feature = mommy.prepare("service.NetworkFeature")
    serializer = NetworkFeatureBaseSerializer(feature)
    result = serializer.data
    assert result, "Serializer should serialize without crashing"

    # Check embeddings
    assert isinstance(result["flags"], list)


def test_blackholing_feature_serializer():
    """Test serializing a blackholing feature"""
    feature = mommy.prepare("service.BlackholingNetworkFeature")
    serializer = BlackholingNetworkFeatureSerializer(feature)
    assert serializer.data, "Serializer should serialize without crashing"


@pytest.mark.django_db
def test_route_server_feature_serializer():
    """Test route server feature serialization"""
    ip4 = mommy.make("ipam.IpAddress", version=4)
    feature = mommy.make("service.RouteServerNetworkFeature",
                         ip_addresses=[ip4])
    serializer = RouteServerNetworkFeatureSerializer(feature)
    result = serializer.data

    assert result, "Serializer should produce data"

    # Check embeddings
    assert isinstance(result["ips"], list)


@pytest.mark.django_db
def test_ixprouter_feature_serializer():
    """Test IXPRouter feature serialization"""
    ip4 = mommy.make("ipam.IpAddress", version=4)
    feature = mommy.make("service.IXPRouterNetworkFeature",
                         ip_addresses=[ip4])
    serializer = IXPRouterNetworkFeatureSerializer(feature)
    result = serializer.data

    assert result, "Serializer should produce data"

    # Check embeddings
    assert isinstance(result["ips"], list)


@pytest.mark.django_db
def test_polymorphic_feature_serializer():
    """Test polymorphic feature serialization"""
    features = [
        mommy.make("service.BlackholingNetworkFeature"),
        mommy.make("service.IXPRouterNetworkFeature"),
        mommy.make("service.RouteServerNetworkFeature"),
    ]

    serializer = NetworkFeatureSerializer(features, many=True)
    result = serializer.data

    assert result, "Serializer should serialize without crashing"
    assert len(result) == len(features), \
        "Serializer should serialize all features"


