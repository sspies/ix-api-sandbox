
"""
Test JEA Access Views
"""

import pytest
import mock
from model_mommy import mommy

from jea.api.v1.tests import authorized_requests
from jea.api.v1.access import views
from jea.access import models as access_models
from jea.catalog import models as catalog_models

#
# Demarcs
#
@pytest.mark.django_db
def test_demarcs_view_set__list():
    """List all demarcs"""
    view = views.DemarcsViewSet.as_view({"get": "list"})
    customer = mommy.make("crm.Customer")
    request = authorized_requests.get(customer)

    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_demarcs_view_set__retrieve():
    """Get a specific demarc"""
    view = views.DemarcsViewSet.as_view({"get": "retrieve"})
    customer = mommy.make(
        "crm.Customer")
    demarc = mommy.make(
        "access.DemarcationPoint",
        scoping_customer=customer)
    request = authorized_requests.get(customer)
    response = view(request, pk=demarc.pk)
    assert response.status_code == 200


@mock.patch("jea.catalog.services.pops.get_media_type_availability")
@pytest.mark.django_db
def test_demarcs_view_set__create(get_availablity):
    """Create a demarc"""
    customer = mommy.make(
        "crm.Customer")
    impl_contact = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=customer)
    billing_contact = mommy.make(
        "crm.BillingContact",
        scoping_customer=customer)
    pop = mommy.make(
        "catalog.PointOfPresence")

    get_availablity.return_value = 42

    # Make request
    request = authorized_requests.post(customer, {
        "media_type": catalog_models.MediaType.TYPE_10GBASE_LR.value,
        "contacts": [impl_contact.pk, billing_contact.pk],
        "pop": pop.pk,
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    view = views.DemarcsViewSet.as_view({"post": "create"})
    response = view(request)

    assert response.status_code == 201


@mock.patch("jea.catalog.services.pops.get_media_type_availability")
@pytest.mark.django_db
def test_demarc_view_set__create_with_connection(
        get_availablity,
    ):
    """Create a demarc with a given connection"""
    customer = mommy.make(
        "crm.Customer")
    pop = mommy.make(
        "catalog.PointOfPresence")
    impl_contact = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=customer)
    billing_contact = mommy.make(
        "crm.BillingContact",
        scoping_customer=customer)
    connection = mommy.make(
        "access.Connection",
        scoping_customer=customer)

    get_availablity.return_value = 42

    assert not connection.demarcation_points.all(), \
        "There should not be an assigned demarc"

    # Make request
    request = authorized_requests.post(customer, {
        "media_type": catalog_models.MediaType.TYPE_10GBASE_LR.value,
        "contacts": [impl_contact.pk, billing_contact.pk],
        "pop": pop.pk,
        "connection": connection.pk,
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    view = views.DemarcsViewSet.as_view({"post": "create"})
    response = view(request)

    assert response.status_code == 201

    # Check result
    connection.refresh_from_db()
    assert connection.demarcation_points.all(), \
        "There should be an assigned demarc"


@pytest.mark.django_db
def test_demarcs_view_set__update():
    """Test updating a demarc"""
    customer = mommy.make(
        "crm.Customer")
    demarc = mommy.make(
        "access.DemarcationPoint",
        scoping_customer=customer)
    connection = mommy.make(
        "access.Connection",
        scoping_customer=customer)
    impl_contact = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=customer)
    billing_contact = mommy.make(
        "crm.BillingContact",
        scoping_customer=customer)


    # Make update
    request = authorized_requests.put(customer, {
        "contacts": [impl_contact.pk, billing_contact.pk],
        "connection": connection.pk,
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })

    view = views.DemarcsViewSet.as_view({"put": "update"})
    response = view(request, pk=demarc.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_demarcs_view_set__partial_update():
    """Test updating a demarc"""
    customer = mommy.make(
        "crm.Customer")
    demarc = mommy.make(
        "access.DemarcationPoint",
        scoping_customer=customer)
    connection = mommy.make(
        "access.Connection",
        scoping_customer=customer)

    # Make partial update
    request = authorized_requests.patch(customer, {
        "connection": connection.pk,
    })

    view = views.DemarcsViewSet.as_view({"patch": "partial_update"})
    response = view(request, pk=demarc.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_demarcs_view_set__destroy():
    """Test demarc deallocation"""
    customer = mommy.make(
        "crm.Customer")
    demarc = mommy.make(
        "access.DemarcationPoint",
        scoping_customer=customer)

    # Make request
    request = authorized_requests.delete(customer)
    view = views.DemarcsViewSet.as_view({"delete": "destroy"})
    response = view(request, pk=demarc.pk)

    assert response.status_code == 200

#
# Connections
#

@pytest.mark.django_db
def test_connections_view_set__list():
    """Test listing connections"""
    customer = mommy.make(
        "crm.Customer")
    request = authorized_requests.get(customer)
    view = views.ConnectionsViewSet.as_view({"get": "list"})
    response = view(request)

    assert response.status_code == 200


@pytest.mark.django_db
def test_connections_view_set__retrieve():
    """Test listing connections"""
    customer = mommy.make(
        "crm.Customer")
    connection = mommy.make(
        "access.Connection",
        scoping_customer=customer)
    request = authorized_requests.get(customer)
    view = views.ConnectionsViewSet.as_view({"get": "retrieve"})
    response = view(request, pk=connection.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_connections_view_set__create():
    """Test connection create"""
    customer = mommy.make(
        "crm.Customer")
    impl_contact = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=customer)
    billing_contact = mommy.make(
        "crm.BillingContact",
        scoping_customer=customer)

    request = authorized_requests.post(customer, {
        "mode": "lag_lacp",
        "speed": 2500,
        "contacts": [billing_contact.pk, impl_contact.pk],
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
    })
    view = views.ConnectionsViewSet.as_view({"post": "create"})
    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_connections_view_set__update():
    """Test updating a connection"""
    customer = mommy.make(
        "crm.Customer")
    impl_contact = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=customer)
    billing_contact = mommy.make(
        "crm.BillingContact",
        scoping_customer=customer)
    connection = mommy.make(
        "access.Connection",
        scoping_customer=customer)

    request = authorized_requests.put(customer, {
        "mode": "lag_lacp",
        "lacp_timeout": "slow",
        "speed": 5000,
        "contacts": [impl_contact.pk, billing_contact.pk],
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "external_ref": "Test2048",
    })
    view = views.ConnectionsViewSet.as_view({"put": "update"})
    response = view(request, pk=connection.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_connections_view_set__partial_update():
    """Test partially updating a connection"""
    customer = mommy.make(
        "crm.Customer")
    connection = mommy.make(
        "access.Connection",
        scoping_customer=customer)

    request = authorized_requests.patch(customer, {
        "lacp_timeout": "fast",
    })
    view = views.ConnectionsViewSet.as_view({"patch": "partial_update"})
    response = view(request, pk=connection.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_connections_view_set__destroy():
    """Destroy a connection"""
    customer = mommy.make(
        "crm.Customer")
    connection = mommy.make(
        "access.Connection",
        managing_customer=customer,
        consuming_customer=customer,
        scoping_customer=customer)

    request = authorized_requests.delete(customer)
    view = views.ConnectionsViewSet.as_view({"delete": "destroy"})
    response = view(request, pk=connection.pk)
    assert response.status_code == 200


#
# Configurations:
# NetworkServiceConfig
#
@pytest.mark.django_db
def test_network_service_configs_view_set__list():
    """List all the network service configs"""
    customer = mommy.make("crm.Customer")

    request = authorized_requests.get(customer)
    view = views.NetworkServiceConfigsViewSet.as_view({"get": "list"})
    response = view(request)

    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_configs_view_set__retrieve():
    """Get a specific network service config"""
    customer = mommy.make(
        "crm.Customer")
    config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_customer=customer)

    request = authorized_requests.get(customer)
    view = views.NetworkServiceConfigsViewSet.as_view({"get": "retrieve"})
    response = view(request, pk=config.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_configs_view_set__create():
    """Create a network service config"""
    customer = mommy.make(
        "crm.Customer")
    contact = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=customer)
    connection = mommy.make(
        "access.Connection",
        scoping_customer=customer)
    net_addr = mommy.make(
        "ipam.IpAddress",
        address="10.23.42.0",
        version=4,
        prefix_length=24)
    network_service = mommy.make(
        "service.ExchangeLanNetworkService",
        ip_addresses=[net_addr],
        required_contact_types=["implementation"])

    # Make a create request
    view = views.NetworkServiceConfigsViewSet.as_view({"post": "create"})
    request = authorized_requests.post(customer, {
        "type": "exchange_lan",
        "connection": connection.pk,
        "network_service": network_service.pk,
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "contacts": [contact.pk],
        "outer_vlan": [[0, 4094]],
    })
    response = view(request)
    assert response.status_code == 201


@pytest.mark.django_db
def test_network_service_configs_view_set__update():
    """Update a network service config"""
    customer = mommy.make(
        "crm.Customer")
    contact = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=customer)
    connection = mommy.make(
        "access.Connection",
        scoping_customer=customer)
    service = mommy.make(
        "service.ExchangeLanNetworkService",
        required_contact_types=["implementation"])
    config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        network_service=service,
        scoping_customer=customer)
    mac_addr = mommy.make(
        "ipam.MacAddress",
        scoping_customer=customer)

    view = views.NetworkServiceConfigsViewSet.as_view({"put": "update"})
    request = authorized_requests.put(customer, {
        "macs": [mac_addr.pk],
        "connection": connection.pk,
        "network_service": config.network_service.pk,
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "contacts": [contact.pk],
        "outer_vlan": [[0, 4095]],
    })
    response = view(request, pk=config.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_configs_view_set__partial_update():
    """Partially update a network service config"""
    customer = mommy.make(
        "crm.Customer")
    contact = mommy.make(
        "crm.ImplementationContact",
        scoping_customer=customer)
    service = mommy.make(
        "service.ExchangeLanNetworkService",
        required_contact_types=["implementation"])
    config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_customer=customer,
        network_service=service)

    view = views.NetworkServiceConfigsViewSet.as_view({
        "patch": "partial_update",
    })
    request = authorized_requests.patch(customer, {
        "contacts": [contact.pk],
    })
    response = view(request, pk=config.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_service_configs_view_set__destroy():
    """Delete a network service config"""
    customer = mommy.make(
        "crm.Customer")
    config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_customer=customer)

    view = views.NetworkServiceConfigsViewSet.as_view({"delete": "destroy"})
    request = authorized_requests.delete(customer)
    response = view(request, pk=config.pk)
    assert response.status_code == 200

#
# Network Feature Configs
#
@pytest.mark.django_db
def test_network_feature_configs_view_set__list():
    """Test list in feature config view set"""
    customer = mommy.make("crm.Customer")
    view = views.NetworkFeatureConfigsViewSet.as_view({"get": "list"})
    request = authorized_requests.get(customer)
    response = view(request)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_feature_configs_view_set__retrieve():
    """Test getting a feature config"""
    customer = mommy.make(
        "crm.Customer")
    config = mommy.make(
        "access.RouteServerNetworkFeatureConfig",
        scoping_customer=customer)
    view = views.NetworkFeatureConfigsViewSet.as_view({"get": "retrieve"})
    request = authorized_requests.get(customer)
    response = view(request, pk=config.pk)
    assert response.status_code == 200


@pytest.mark.django_db
def test_network_feature_configs_view_set__create():
    """Test creating a network service feature"""
    customer = mommy.make(
        "crm.Customer")
    network_service = mommy.make(
        "service.ExchangeLanNetworkService")
    network_feature = mommy.make(
        "service.RouteServerNetworkFeature",
        network_service=network_service,
        address_families=['af_inet'],
        required_contact_types=[],
        available_bgp_session_types=[
            access_models.BGPSessionType.TYPE_PASSIVE.value,
        ])
    network_service_config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_customer=customer,
        network_service=network_service)

    # Create request
    view = views.NetworkFeatureConfigsViewSet.as_view({"post": "create"})
    request = authorized_requests.post(customer, {
        "type": "route_server",
        "network_feature": network_feature.pk,
        "network_service_config": network_service_config.pk,
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "asn": 23042,
        "as_set_v4": "AS-FOO",
        "session_mode": network_feature.session_mode.value,
        "bgp_session_type": 
            access_models.BGPSessionType.TYPE_PASSIVE.value,
        "contacts": [],
    })
    response = view(request)

    assert response.status_code == 201

@pytest.mark.django_db
def test_network_feature_configs_view_set__update():
    """Update a network feature config"""
    customer = mommy.make(
        "crm.Customer")
    network_service = mommy.make(
        "service.ExchangeLanNetworkService")
    network_feature = mommy.make(
        "service.RouteServerNetworkFeature",
        address_families=['af_inet'],
        network_service=network_service)
    network_service_config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_customer=customer,
        network_service=network_service)
    network_feature_config = mommy.make(
        "access.RouteServerNetworkFeatureConfig",
        scoping_customer=customer,
        network_feature=network_feature,
        network_service_config=network_service_config)

    # Create request
    view = views.NetworkFeatureConfigsViewSet.as_view({"put": "update"})
    request = authorized_requests.put(customer, {
        "managing_customer": customer.pk,
        "consuming_customer": customer.pk,
        "asn": 23042,
        "as_set_v4": "AS-FOO",
        "session_mode": network_feature.session_mode.value,
        "bgp_session_type": 
            access_models.BGPSessionType.TYPE_ACTIVE.value,
    })
    response = view(request, pk=network_feature_config.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_network_feature_configs_view_set__partial_update():
    """Update a network feature config with partial changes"""
    customer = mommy.make(
        "crm.Customer")
    network_service = mommy.make(
        "service.ExchangeLanNetworkService")
    network_feature = mommy.make(
        "service.RouteServerNetworkFeature",
        network_service=network_service,
        address_families=['af_inet6'])
    network_service_config = mommy.make(
        "access.ExchangeLanNetworkServiceConfig",
        scoping_customer=customer,
        network_service=network_service)
    network_feature_config = mommy.make(
        "access.RouteServerNetworkFeatureConfig",
        scoping_customer=customer,
        as_set_v6="AS-BAR",
        network_feature=network_feature,
        network_service_config=network_service_config)

    # Create request
    view = views.NetworkFeatureConfigsViewSet.as_view({
        "patch": "partial_update",
    })
    request = authorized_requests.patch(customer, {
        "asn": 23042,
        "as_set_v6": "AS-FOO",
    })
    response = view(request, pk=network_feature_config.pk)

    assert response.status_code == 200


@pytest.mark.django_db
def test_network_feature_configs_view_set__destroy():
    """Remove a network feature config"""
    customer = mommy.make("crm.Customer")
    network_feature_config = mommy.make(
        "access.RouteServerNetworkFeatureConfig",
        scoping_customer=customer)

    view = views.NetworkFeatureConfigsViewSet.as_view({"delete": "destroy"})
    request = authorized_requests.delete(customer)
    response = view(request, pk=network_feature_config.pk)

    assert response.status_code == 200
