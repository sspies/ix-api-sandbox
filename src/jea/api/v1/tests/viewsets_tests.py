
"""
Test JEA ViewSet extensions

"""

from django.http import HttpRequest

from jea.api.v1.response import ApiResponse, ApiError, ApiSuccess
from jea.api.v1.viewsets import JEAViewSet


#
# Assertation helpers
#
def _check_response_wrapping(response):
    """Test if the response is correctly wrapped"""
    assert isinstance(response, ApiResponse), \
        "JEA ViewSets results should be wrappen in an ApiResponse object"


#
# API Response tests
#
def test_viewset_response_enveloping():
    """The viewsets response should be wrapped"""

    class TestViewSet(JEAViewSet):
        def list(self, request):
            return ApiSuccess("everything's fine!")

        def retrieve(self, request, pk=None):
            return ApiError("Nothing's right")

        def update(self, request, pk=None):
            return 42

    viewset = TestViewSet()
    req = HttpRequest()

    # Execute handlers
    handlers = ['list', 'retrieve', 'update']
    for method in handlers:
        handler = getattr(viewset, method)

        response = viewset.list(req)

        # Check wrapping and envelope
        _check_response_wrapping(response)


def test_viewsets_exception_handling():
    """Exceptions within a viewset should create an error response"""
    pass
