
import pytest
from model_mommy import mommy

from rest_framework.test import APIRequestFactory

from jea.auth.services import token_authentication as token_authentication_svc
from jea.api.v1.auth.handlers import (
    JWTAuthentication,
    ApiSessionAuthentication,
)
from jea.api.v1.auth.users import ApiUser



@pytest.mark.django_db
def test_jwt_authentication_authenticate():
    """Test authenticating a request"""
    # Create customer and subcustomer
    customer = mommy.make("crm.Customer")
    access_token, _ = token_authentication_svc.issue_tokens(
        customer.pk)

    # Make request
    request = APIRequestFactory().get("/api/example")
    request.META["HTTP_AUTHORIZATION"] = b"Bearer " + \
        access_token.encode('utf-8')

    # Authenticate request
    handler = JWTAuthentication()
    user, token = handler.authenticate(request)

    assert isinstance(user, ApiUser), \
        "Expected user to be a valid API user"

    assert user.customer.pk == customer.pk


@pytest.mark.django_db
def test_api_session_authentication_authenticate():
    """Test authenticating a request"""
    # Create customer and subcustomer
    user = mommy.make("jea_auth.User")
    customer = mommy.make("crm.Customer")

    # Make request
    request = APIRequestFactory().get("/api/example")
    request.user = user
    request.session = {
        "auth_customer_id": customer.id,
    }
    request._request = request

    # Authenticate request
    handler = ApiSessionAuthentication()
    result = handler.authenticate(request)
    assert result

    user, _ = result
    assert isinstance(user, ApiUser), \
        "Expected user to be a valid API user"

    assert user.customer.id == customer.id

