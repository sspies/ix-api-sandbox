
"""
Service Access Serializers
--------------------------

Serialize access objects like connections or port demarcs,
network service access objects.
"""


from rest_framework import serializers

from jea.api.v1.serializers import (
    JeaPrimaryKeyRelatedField,
    EnumField,
    PolymorphicSerializer,
)
from jea.api.v1.eventmachine.serializers import (
    StatefulObjectSerializer,
)
from jea.api.v1.crm.serializers import (
    CustomerSerializer,
    OwnableObjectSerializer,
    InvoiceableObjectSerializer,
    ImplementationContactSerializer,
)
from jea.api.v1.catalog.serializers import (
    PointOfPresenceSerializer,
)
from jea.catalog.models import (
    MediaType,
)
from jea.access.models import (
    ConnectionMode,
    LACPTimeout,
)
from jea.ipam import models as ipam_models
from jea.service import models as service_models
from jea.access import models as access_models
from jea.catalog import models as catalog_models
from jea.crm import models as crm_models


#
# Connections
#
class ConnectionBaseSerializer(
        OwnableObjectSerializer,
        InvoiceableObjectSerializer,
        serializers.Serializer,
    ):
    id = JeaPrimaryKeyRelatedField(read_only=True)
    mode = EnumField(
        ConnectionMode,
        help_text="""
            Sets the mode of the connection. The mode can be:

            - `lag_lacp`: connection is build as a LAG with LACP enabled
            - `lag_static`: connection is build as LAG with static configuration
            - `flex_ethernet`: connect is build as a FlexEthernet channel
            - `standalone`: only one demarc is allowed in this connection without
            any bundling.

            Example: "lag_lacp"
        """)
    lacp_timeout = EnumField(
        LACPTimeout,
        required=False,
        default=None,
        allow_null=True,
        help_text="""
            This sets the LACP Timeout mode. Both ends of the connections need
            to be configured the same.

            Example: "slow"
        """)

    speed = serializers.IntegerField(
        allow_null=True,
        required=False,
        min_value=0,
        help_text="""
            Shows the total bandwidth of the connection in Mbit/s.

            Example: "20000"
        """)


class ConnectionSerializer(
        StatefulObjectSerializer,
        ConnectionBaseSerializer,
    ):
    """Connection"""
    name = serializers.CharField(max_length=80,
                                 required=False,
                                 allow_blank=True)

    demarcs = JeaPrimaryKeyRelatedField(
        source="demarcation_points",
        many=True,
        required=False,
        queryset=access_models.DemarcationPoint.objects.all(),
        help_text="""
            Reference to the demarcs belonging to this connection. Typically
            all demarcs within one connection are distributed over the same
            device.

            Example: ["ID23", "42", "5"]
        """)


class ConnectionRequestSerializer(
        ConnectionBaseSerializer,
    ):
    """Connection Request"""


class ConnectionUpdateSerializer(
        ConnectionBaseSerializer,
    ):
    """Connection Update"""

#
# Demarcation Points
#
class DemarcationPointBaseSerializer(
        OwnableObjectSerializer,
        InvoiceableObjectSerializer,
        serializers.Serializer,
    ):
    """Demarcation Point Base"""
    id = JeaPrimaryKeyRelatedField(read_only=True)

    connection = JeaPrimaryKeyRelatedField(
        allow_null=True,
        required=False,
        queryset=access_models.Connection.objects.all())

    speed = serializers.IntegerField(
        read_only=True,
        min_value=0,
        default=None,
        allow_null=True)


class DemarcationPointSerializer(
        StatefulObjectSerializer,
        DemarcationPointBaseSerializer,
    ):
    """Demarc"""
    name = serializers.CharField(
        max_length=80,
        read_only=True,
        default="",
        help_text="""
            Name of the demarcation point (set by the IXP)
        """)
    media_type = serializers.CharField(
        max_length=20,
        help_text="""
            The media type of the interface.
            See the device's capabilities to see what types
            are available.

            Example: "10GBASE-LR"
        """)

    # Relations Relation
    pop = JeaPrimaryKeyRelatedField(
        source="point_of_presence",
        queryset=catalog_models.PointOfPresence.objects.all())


class DemarcationPointRequestSerializer(
        DemarcationPointBaseSerializer,
        serializers.Serializer,
    ):
    """Demarc Request"""
    # Relations Relation
    pop = JeaPrimaryKeyRelatedField(
        source="point_of_presence",
        queryset=catalog_models.PointOfPresence.objects.all())

    media_type = serializers.CharField(
        max_length=20,
        help_text="""
            The media type of the interface.
            See the device's capabilities to see what types
            are available.

            Example: "10GBASE-LR"
        """)

class DemarcationPointUpdateSerializer(
        DemarcationPointBaseSerializer,
        serializers.Serializer,
    ):
    """Demarc Update"""


#
# Network Service Configs
#
class NetworkServiceConfigBaseSerializer(
        OwnableObjectSerializer,
        InvoiceableObjectSerializer,
        serializers.Serializer,
    ):
    """Network Service Config"""
    id = JeaPrimaryKeyRelatedField(read_only=True)

    inner_vlan = serializers.IntegerField(
        allow_null=True,
        required=False,
        default=None,
        min_value=0,
        max_value=4096,
        help_text="""
            This is a VLAN ID.
        """)

    capacity = serializers.IntegerField(
        required=False,
        default=None,
        min_value=1,
        allow_null=True,
        help_text="""
            The capacity of the service in Mbps. If set to Null,
            the maximum capacity will be used.
            That means, the service can consume up
            to the total bandwidth of the `connection`.

            Typically the service is charged based on the capacity.
        """)

    connection = JeaPrimaryKeyRelatedField(
        queryset=access_models.Connection.objects.all(),
        help_text="""
            The id of the connection to use for this service config. This
            associates the service to a LAG. All traffic comming in on the
            connection with the correct VLAN-ID will be transported to this
            service.
        """)

    network_feature_configs = JeaPrimaryKeyRelatedField(
        many=True,
        read_only=True,
        help_text="""
            A list of ids of network feature configurations.

            Example: ["12356", "43829"]
        """)


class NetworkServiceConfigInputBaseSerializer(
        OwnableObjectSerializer,
        serializers.Serializer,
    ):
    """Network Service Config Request"""
    network_service = JeaPrimaryKeyRelatedField(
        queryset=service_models.NetworkService.objects.all(),
        help_text="""
            The id of the network service to configure.
        """)


    # When requesting a new network service config,
    # we accept a set of vlan ranges.
    outer_vlan = serializers.ListField(
        required=True,
        child=serializers.ListField(
            child=serializers.IntegerField(
                min_value=0,
                max_value=4094),
            min_length=1,
            max_length=2),
        min_length=1,
        help_text="""
            Request a vlan from a set of ranges.
            A range is represented as a list of integers.
            The range has a max length of 2.

            A set of ranges is a list of lists.

            In case only one bound in the range is present,
            it is interpreted as the exact value. That means that `[23]` is
            equivalent to `[23, 23]`.

            To just let the platform choose for you, provide all
            possible ranges. `[ [0, 4096] ]`.

            Example: [[0, 4096]]
        """)

class NetworkServiceConfigUpdateBaseSerializer(
        OwnableObjectSerializer,
        serializers.Serializer,
    ):
    """Network Service Config Update"""
    # See network service config input
    outer_vlan = serializers.ListField(
        child=serializers.ListField(
            child=serializers.IntegerField(
                min_value=0,
                max_value=4096),
            default=[],
            min_length=1,
            max_length=2),
        default=[],
        min_length=1,
        help_text="""
            Request a vlan from a set of ranges.
            A range is represented as a list of integers.
            The range has a max length of 2.

            A set of ranges is a list of lists.

            In case only one bound in the range is provided,
            it is interpreted as the exact value. `[23]` is
            equivalent to `[23, 23]`.

            To just let the platform choose for you, provide all
            possible ranges. `[ [0, 4096] ]`.

            Example: [[0, 4096]]
        """)


class NetworkServiceConfigOutputBaseSerializer(
        StatefulObjectSerializer,
    ):
    """Network Service Config"""
    network_service = JeaPrimaryKeyRelatedField(
        queryset=service_models.NetworkService.objects.all(),
        help_text="""
            The id of the configured network service.
        """)

    outer_vlan = serializers.IntegerField(
        min_value=0,
        max_value=4096,
        help_text="""
            A vlan id, where `0` is untagged.
            Any value > 0 will be the tagged vlan.

            Example: 300
        """)

# Exchange Lan

class ExchangeLanNetworkServiceConfigBaseSerializer(
        NetworkServiceConfigBaseSerializer,
    ):
    """Exchange Lan Network Service Config"""
    asns = serializers.ListField(
        child=serializers.IntegerField(min_value=0, max_value=4294967295),
        default=[],
        min_length=0,
        max_length=20)

    macs = JeaPrimaryKeyRelatedField(
        source="mac_addresses",
        required=False,
        queryset=ipam_models.MacAddress.objects.all(),
        many=True)

    ips = JeaPrimaryKeyRelatedField(
        source="ip_addresses",
        many=True, read_only=True)

    # Mark as polymorphic to inject a 'type'
    __polymorphic_type__ = \
        access_models.NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN


class ExchangeLanNetworkServiceConfigInputSerializer(
        NetworkServiceConfigInputBaseSerializer,
        ExchangeLanNetworkServiceConfigBaseSerializer,
    ):
    """Exchange Lan Network Service Config Request"""


class ExchangeLanNetworkServiceConfigUpdateSerializer(
        NetworkServiceConfigUpdateBaseSerializer,
        ExchangeLanNetworkServiceConfigBaseSerializer,
    ):
    """Exchange Lan Network Service Config Update"""


class ExchangeLanNetworkServiceConfigSerializer(
        NetworkServiceConfigOutputBaseSerializer,
        ExchangeLanNetworkServiceConfigBaseSerializer,
    ):
    """Exchange Lan Network Service Config"""

# Closed User Group

class ClosedUserGroupNetworkServiceConfigBaseSerializer(
        NetworkServiceConfigBaseSerializer,
    ):
    """Closed User Group Network Service Config"""
    macs = JeaPrimaryKeyRelatedField(
        source="mac_addresses",
        many=True, read_only=True)

    __polymorphic_type__ = \
        access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP
    __hidden_in_docs__ = True


class ClosedUserGroupNetworkServiceConfigInputSerializer(
        NetworkServiceConfigInputBaseSerializer,
        ClosedUserGroupNetworkServiceConfigBaseSerializer,
    ):
    """Closed User Group Network Service Config Request"""


class ClosedUserGroupNetworkServiceConfigUpdateSerializer(
        NetworkServiceConfigUpdateBaseSerializer,
        ClosedUserGroupNetworkServiceConfigBaseSerializer,
    ):
    """Closed User Group Network Service Update"""


class ClosedUserGroupNetworkServiceConfigSerializer(
        NetworkServiceConfigOutputBaseSerializer,
        ClosedUserGroupNetworkServiceConfigBaseSerializer,
    ):
    """Closed User Group Network Service Config"""

# E-Line

class ELineNetworkServiceConfigBaseSerializer(
        NetworkServiceConfigBaseSerializer,
    ):
    """ELine Network Service Config"""
    __polymorphic_type__ = \
        access_models.NETWORK_SERVICE_CONFIG_TYPE_ELINE
    __hidden_in_docs__ = True


class ELineNetworkServiceConfigInputSerializer(
        NetworkServiceConfigInputBaseSerializer,
        ELineNetworkServiceConfigBaseSerializer,
    ):
    """ELine Network Service Config Request"""

class ELineNetworkServiceConfigUpdateSerializer(
        NetworkServiceConfigUpdateBaseSerializer,
        ELineNetworkServiceConfigBaseSerializer,
    ):
    """ELine Network Service Config Update"""

class ELineNetworkServiceConfigSerializer(
        NetworkServiceConfigOutputBaseSerializer,
        ELineNetworkServiceConfigBaseSerializer,
    ):
    """ELine Network Service Config"""

# Cloud

class CloudNetworkServiceConfigBaseSerializer(
        NetworkServiceConfigBaseSerializer,
    ):
    """Cloud Network Service Config"""
    cloud_key = serializers.CharField(max_length=512)

    __hidden_in_docs__ = True
    __polymorphic_type__ = \
        access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOUD


class CloudNetworkServiceConfigInputSerializer(
        NetworkServiceConfigInputBaseSerializer,
        CloudNetworkServiceConfigBaseSerializer,
    ):
    """Cloud Network Service Config Request"""


class CloudNetworkServiceConfigUpdateSerializer(
        NetworkServiceConfigUpdateBaseSerializer,
        CloudNetworkServiceConfigBaseSerializer,
    ):
    """Cloud Network Service Config Update"""


class CloudNetworkServiceConfigSerializer(
        NetworkServiceConfigOutputBaseSerializer,
        CloudNetworkServiceConfigBaseSerializer,
    ):
    """Cloud Network Service Config"""


class NetworkServiceConfigInputSerializer(PolymorphicSerializer):
    """Polymorhic Network Service Config Request"""
    serializer_classes = {
        access_models.NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkServiceConfigInputSerializer,
        access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP:
            ClosedUserGroupNetworkServiceConfigInputSerializer,
        access_models.NETWORK_SERVICE_CONFIG_TYPE_ELINE:
            ELineNetworkServiceConfigInputSerializer,
        access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOUD:
            CloudNetworkServiceConfigInputSerializer,
    }

    entity_types = access_models.NETWORK_SERVICE_CONFIG_TYPES


class NetworkServiceConfigUpdateSerializer(PolymorphicSerializer):
    """Polymorphic Network Service Config"""
    serializer_classes = {
        access_models.NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkServiceConfigUpdateSerializer,
        access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP:
            ClosedUserGroupNetworkServiceConfigUpdateSerializer,
        access_models.NETWORK_SERVICE_CONFIG_TYPE_ELINE:
            ELineNetworkServiceConfigUpdateSerializer,
        access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOUD:
            CloudNetworkServiceConfigUpdateSerializer,
    }

    entity_types = access_models.NETWORK_SERVICE_CONFIG_TYPES


class NetworkServiceConfigSerializer(PolymorphicSerializer):
    """Polymorphic Network Service Config"""
    serializer_classes = {
        access_models.NETWORK_SERVICE_CONFIG_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkServiceConfigSerializer,
        access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOSED_USER_GROUP:
            ClosedUserGroupNetworkServiceConfigSerializer,
        access_models.NETWORK_SERVICE_CONFIG_TYPE_ELINE:
            ELineNetworkServiceConfigSerializer,
        access_models.NETWORK_SERVICE_CONFIG_TYPE_CLOUD:
            CloudNetworkServiceConfigSerializer,
    }

    entity_types = access_models.NETWORK_SERVICE_CONFIG_TYPES

#
# Feature Configurations
#
class NetworkFeatureConfigBaseSerializer(
        OwnableObjectSerializer,
        InvoiceableObjectSerializer,
        serializers.Serializer,
    ):
    """A feature access base serializer"""
    id = JeaPrimaryKeyRelatedField(read_only=True)

    network_feature = JeaPrimaryKeyRelatedField(
        queryset=service_models.NetworkFeature.objects.all())
    network_service_config = JeaPrimaryKeyRelatedField(
        queryset=access_models.NetworkServiceConfig.objects.all())


class NetworkFeatureConfigUpdateBaseSerializer(
        OwnableObjectSerializer,
        serializers.Serializer,
    ):
    """Network Feature Configu Updates Base"""


class RouteServerNetworkFeatureConfigBaseSerializer(serializers.Serializer):
    """Routeserver Config Feature Serializer"""
    asn = serializers.IntegerField(
        min_value=0,
        max_value=4294967295,
        help_text="""
            The ASN of the peer.

            Example: 4200000023
        """)
    password = serializers.CharField(
        required=False,
        default="",
        max_length=128,
        allow_blank=True,
        allow_null=False,
        help_text="""
            The cleartext BGP session password
            Example: bgp-session-test-23
        """)

    as_set_v4 = serializers.CharField(
        max_length=100,
        required=False,
        allow_null=True,
        help_text="""
            AS-Set of the customer for IPv4 prefix filtering.
            This is used to generate
            filters on the router servers.
            Only valid referenced prefixes within the AS-Set
            are allowed inbound to the route server. All other routes are
            filtered.

            This field is *required* with session mode `public`
            if the route server network feature supports the `af_inet`
            address family.

            Important: The format has to be: "AS-SET@IRR". IRR is the database
            where the AS-SET is registred. Typically used IRR's are RADB, RIPE,
            NTTCOM, APNIC, ALTDB, LEVEL3, ARIN, AFRINIC, LACNIC

            Example: MOON-AS@RIPE
        """)
    as_set_v6 = serializers.CharField(
        max_length=100,
        required=False,
        allow_null=True,
        help_text="""
            AS-Set of the customer for IPv6. This is used to generate filters
            on the router servers. Only valid referenced prefixes within
            the AS-Set are allowed inbound to the route server.
            All other routes are filtered.

            This field is *required* with session mode `public`
            if the route server network feature supports the `af_inet6`
            address family.

            Important: The format has to be: "AS-SET@IRR". IRR is the database
            where the AS-SET is registred. Typically used IRR's are RADB, RIPE,
            NTTCOM, APNIC, ALTDB, LEVEL3, ARIN, AFRINIC, LACNIC

            Example: MOON-AS@RIPE
        """)
    max_prefix_v4 = serializers.IntegerField(
        required=False,
        allow_null=True,
        min_value=0,
        help_text="""
            Announcing more than `max_prefix` IPv4 prefixes the bgp
            session will be droped.

            Example: 5000
        """)
    max_prefix_v6 = serializers.IntegerField(
        required=False,
        allow_null=True,
        min_value=0,
        help_text="""
            Announcing more than `max_prefix` IPv6 prefixes the bgp
            session will be droped.

            Example: 5000
        """)
    insert_ixp_asn = serializers.BooleanField(
        default=True,
        required=False,
        help_text="""
            Insert the ASN of the IXP into the AS path. This function is only
            used in special cases. In 99% of all cases, it should be false.

            Example: false
        """)

    session_mode = EnumField(
        access_models.RouteServerSessionMode,
        help_text="""
            Set the session mode with the routeserver.

            Example: public
        """)
    bgp_session_type = EnumField(
        access_models.BGPSessionType,
        help_text="""
            The session type describes which of the both parties will open the
            connection. If set to passive, the customer router needs to open
            the connection. If its set to active, the route server will open
            the connection. The standard behavior on most IX's is passive.

            Example: passive
        """)

    __polymorphic_type__ = \
        access_models.NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER


class RouteServerNetworkFeatureConfigUpdateSerializer(
        NetworkFeatureConfigUpdateBaseSerializer,
        RouteServerNetworkFeatureConfigBaseSerializer,
    ):
    """Route Server Network Feature Config Update"""


class RouteServerNetworkFeatureConfigInputSerializer(
        NetworkFeatureConfigBaseSerializer,
        RouteServerNetworkFeatureConfigBaseSerializer,
    ):
    """Route Server Network Feature Config Request"""


class RouteServerNetworkFeatureConfigSerializer(
        StatefulObjectSerializer,
        NetworkFeatureConfigBaseSerializer,
        RouteServerNetworkFeatureConfigBaseSerializer,
    ):
    """Route Server Network Feature Config"""


class IXPRouterNetworkFeatureConfigBaseSerializer(serializers.Serializer):
    """IXP Router Feature Config"""
    max_prefix = serializers.IntegerField(min_value=0)
    password = serializers.CharField(
        required=False,
        default=None,
        max_length=128,
        allow_blank=True,
        allow_null=True)
    bgp_session_type = EnumField(access_models.BGPSessionType)

    __hidden_in_docs__ = True
    __polymorphic_type__ = \
        access_models.NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER


class IXPRouterNetworkFeatureConfigUpdateSerializer(
        NetworkFeatureConfigUpdateBaseSerializer,
        IXPRouterNetworkFeatureConfigBaseSerializer,
    ):
    """IXP Router Network Feature Config Update"""


class IXPRouterNetworkFeatureConfigInputSerializer(
        NetworkFeatureConfigBaseSerializer,
        IXPRouterNetworkFeatureConfigBaseSerializer,
    ):
    """IXP Router Network Feature Config Request"""


class IXPRouterNetworkFeatureConfigSerializer(
        StatefulObjectSerializer,
        NetworkFeatureConfigBaseSerializer,
        IXPRouterNetworkFeatureConfigBaseSerializer,
    ):
    """IXP Router Network Feature Config"""


class BlackholingNetworkFeatureConfigBaseSerializer(serializers.Serializer):
    """Blackholing Configuration Base"""
    activated = serializers.BooleanField()
    ixp_specific_configuration = serializers.CharField(
        max_length=2048,
        default=None,
        allow_blank=True,
        allow_null=True)

    filtered_prefixes = JeaPrimaryKeyRelatedField(
        queryset=ipam_models.IpAddress.objects.all(),
        many=True)

    __hidden_in_docs__ = True
    __polymorphic_type__ = \
        access_models.NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING


class BlackholingNetworkFeatureConfigUpdateSerializer(
       NetworkFeatureConfigUpdateBaseSerializer,
       BlackholingNetworkFeatureConfigBaseSerializer,
    ):
    """Blackholing Network Feature Config Update"""


class BlackholingNetworkFeatureConfigInputSerializer(
        NetworkFeatureConfigBaseSerializer,
        BlackholingNetworkFeatureConfigBaseSerializer,
    ):
    """Blackholing Network Feature Config Request"""


class BlackholingNetworkFeatureConfigSerializer(
        StatefulObjectSerializer,
        NetworkFeatureConfigBaseSerializer,
        BlackholingNetworkFeatureConfigBaseSerializer,
    ):
    """Blackholing Network Feature Config"""


class NetworkFeatureConfigSerializer(PolymorphicSerializer):
    """Polymorphic Network Feature Config"""
    serializer_classes = {
        access_models.NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER:
            RouteServerNetworkFeatureConfigSerializer,
        access_models.NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER:
            IXPRouterNetworkFeatureConfigSerializer,
        access_models.NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING:
            BlackholingNetworkFeatureConfigSerializer,
    }

    entity_types = access_models.NETWORK_FEATURE_CONFIG_TYPES


class NetworkFeatureConfigInputSerializer(PolymorphicSerializer):
    """Polymorphic Network Feature Config Request"""
    serializer_classes = {
        access_models.NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER:
            RouteServerNetworkFeatureConfigInputSerializer,
        access_models.NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER:
            IXPRouterNetworkFeatureConfigInputSerializer,
        access_models.NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING:
            BlackholingNetworkFeatureConfigInputSerializer,
    }

    entity_types = access_models.NETWORK_FEATURE_CONFIG_TYPES


class NetworkFeatureConfigUpdateSerializer(PolymorphicSerializer):
    """Polymorphic Network Feauture Config Update"""
    serializer_classes = {
        access_models.NETWORK_FEATURE_CONFIG_TYPE_ROUTESERVER:
            RouteServerNetworkFeatureConfigUpdateSerializer,
        access_models.NETWORK_FEATURE_CONFIG_TYPE_IXPROUTER:
            IXPRouterNetworkFeatureConfigUpdateSerializer,
        access_models.NETWORK_FEATURE_CONFIG_TYPE_BLACKHOLING:
            BlackholingNetworkFeatureConfigUpdateSerializer,
    }

    entity_types = access_models.NETWORK_FEATURE_CONFIG_TYPES


def get_config_update_serializer(config):
    """
    Get the right network service config serializer
    for a given config object.

    :param config: A network service or feature config
    """
    # Network Services
    if isinstance(config, access_models.ExchangeLanNetworkServiceConfig):
        return ExchangeLanNetworkServiceConfigUpdateSerializer
    if isinstance(config, access_models.ClosedUserGroupNetworkServiceConfig):
        return ClosedUserGroupNetworkServiceConfigUpdateSerializer
    if isinstance(config, access_models.ELineNetworkServiceConfig):
        return ELineNetworkServiceConfigUpdateSerializer
    if isinstance(config, access_models.CloudNetworkServiceConfig):
        return CloudNetworkServiceConfigUpdateSerializer

    # Network Features
    if isinstance(config, access_models.BlackholingNetworkFeatureConfig):
        return BlackholingNetworkFeatureConfigUpdateSerializer
    if isinstance(config, access_models.RouteServerNetworkFeatureConfig):
        return RouteServerNetworkFeatureConfigUpdateSerializer
    if isinstance(config, access_models.IXPRouterNetworkFeatureConfig):
        return IXPRouterNetworkFeatureConfigUpdateSerializer

    raise TypeError("Unsupported config type.")
