
"""
Catalog :: Views

Implements catalog related endpoints. For example
for listing facilities and points of presence.
"""

from rest_framework import exceptions, status

from jea.api.v1.errors import problems
from jea.api.v1.permissions import require_customer
from jea.api.v1.response import tags
from jea.api.v1.viewsets import JEAViewSet
from jea.api.v1.response import ApiSuccess, ApiError
from jea.api.v1.catalog.serializers import (
    PointOfPresenceSerializer,
    FacilitySerializer,
    ProductSerializer,
    DeviceSerializer,
)
from jea.catalog.services import (
    devices as devices_svc,
    pops as pops_svc,
    products as products_svc,
    facilities as facilities_svc,
)
from jea.catalog.filters import (
    FacilityFilter,
    DeviceFilter,
    ProductFilter,
    PointOfPresenceFilter,
)


class FacilitiesViewSet(JEAViewSet):
    """
    A `Facility` is a data centre, with a determined physical address,
    from which a defined set of PoPs can be accessed
    """
    serializer_class = FacilitySerializer
    filterset_class = FacilityFilter

    responses = {
        "list": {
            200: FacilitySerializer(many=True),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
            ],
        },
        "read": {
            200: FacilitySerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
            ],
        }
    }

    def list(self, request):
        """Get a (filtered) list of `facilities`."""
        facilities = facilities_svc.get_facilities(
            filters=request.query_params)
        serializer = FacilitySerializer(facilities, many=True)

        return serializer.data

    def retrieve(self, request, pk=None):
        """Retrieve a facility by id"""
        facility = facilities_svc.get_facility(facility=pk)
        serializer = FacilitySerializer(facility)

        return serializer.data


class DevicesViewSet(JEAViewSet):
    """
    A `Device` is a network hardware device, typically a Switch, which
    is located at a specified facility and connected to one or more
    PoPs.

    They may be physically located at their related PoPs or remotely
    available.
    """
    serializer_class = DeviceSerializer
    filterset_class = DeviceFilter

    responses = {
        "list": {
            200: DeviceSerializer(many=True),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
            ],
        },
        "read": {
            200: DeviceSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
            ],
        }
    }


    def list(self, request):
        """List available devices"""
        devices = devices_svc.get_devices(
            filters=request.query_params)
        serializer = DeviceSerializer(devices, many=True)

        return serializer.data

    def retrieve(self, request, pk=None):
        """Get a specific device identified by id"""
        device = devices_svc.get_device(device=pk)
        serializer = DeviceSerializer(device)

        return serializer.data


class PointsOfPresenceViewSet(JEAViewSet):
    """
    A `PoP` is a location within a Facility which is connected to a
    single Network Infrastructure and has defined reachability of other
    facilities.

    A single room may contain multiple PoPs, each linking to a different
    infrastructure.
    """
    serializer_class = PointOfPresenceSerializer
    filterset_class = PointOfPresenceFilter

    responses = {
        "list": {
            200: PointOfPresenceSerializer(many=True),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
            ],
        },
        "read": {
            200: PointOfPresenceSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
            ],
        }
    }

    def list(self, request):
        """List all PoPs"""
        pops = pops_svc.get_pops(filters=request.query_params)
        serializer = PointOfPresenceSerializer(pops, many=True)

        return serializer.data

    def retrieve(self, request, pk=None):
        """Get a demarcation point"""
        pop = pops_svc.get_pop(pop=pk)
        serializer = PointOfPresenceSerializer(pop)

        return serializer.data


class ProductsViewSet(JEAViewSet):
    """
    A `Product` is a network or peering-related product of a defined
    type sold by an IXP to its `Customers`
    """
    serializer_class = ProductSerializer
    filterset_class = ProductFilter

    responses = {
        "list": {
            200: ProductSerializer(many=True),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
            ],
        },
        "read": {
            200: ProductSerializer(),
            "problems": [
                problems.PermissionDeniedProblem(),
                problems.AuthenticationProblem(),
                problems.SignatureExpiredProblem(),
                problems.NotFoundProblem(),
            ],
        }
    }

    def list(self, request):
        """List all (filtered) products available on the platform"""
        products = products_svc.get_products(filters=request.query_params)
        serializer = ProductSerializer(products, many=True)

        return serializer.data

    def retrieve(self, request, pk=None):
        """Get a specific product by id"""
        product = products_svc.get_product(product=pk)
        serializer = ProductSerializer(product)

        return serializer.data
