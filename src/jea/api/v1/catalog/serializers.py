
"""
Catalog Serializers

Serialize catalog entities like facility, products, etc..
"""

from rest_framework import serializers

from jea.api.v1.serializers import (
    JeaPrimaryKeyRelatedField,
    EnumField,
    PolymorphicSerializer,
)
from jea.catalog import models as catalog_models
from jea.catalog.models import (
    CloudProvider,
    FacilityOperator,
    FacilityCluster,
    Facility,
    Device,
    DeviceConnection,
    DeviceCapability,
    PointOfPresence,

    PRODUCT_TYPES,
    INV_PRODUCT_TYPES,
)


class CloudProviderSerializer(serializers.Serializer):
    """Cloud Provider"""
    id = serializers.CharField(label='ID', read_only=True)

    name = serializers.CharField(max_length=80)


class FacilitySerializer(serializers.Serializer):
    """Facility"""
    id = serializers.CharField(
        label='ID',
        read_only=True)

    name = serializers.CharField(
        max_length=80,
        help_text="""
            Name of the Datacenter, how the DC calls itself.

            Example: Crater DC Moon 1
        """)
    metro_area = serializers.CharField(
        max_length=3,
        help_text="""
            3 Letter (IATA) Airport Code of the MetroArea where the DC is in.

            Example: FRA
        """)
    address_country = serializers.CharField(
        max_length=2,
        help_text="""
            ISO 3166-1 alpha-2 country code, for example DE

            example: US
        """)
    address_locality = serializers.CharField(
        max_length=80,
        help_text="""
            The locality/city. For example, Mountain View.
            example: Mountain View
        """)
    address_region = serializers.CharField(
        max_length=80,
        help_text="""
            The region. For example, CA
            example: CA
        """)
    postal_code = serializers.CharField(
        max_length=18,
        help_text="""
            A postal code. For example, 9404
            example: "9409"
        """)
    street_address = serializers.CharField(
        max_length=80,
        help_text="""
            The street address. For example, 1600 Amphitheatre Pkwy.
            example: 1600 Amphitheatre Pkwy.
        """)

    peeringdb_facility_id = serializers.IntegerField(
        allow_null=True,
        max_value=2147483647,
        min_value=0,
        required=False,
        help_text="""
            [PeeringDB](https://www.peeringdb.com) facitlity ID,
            can be extracted from the url https://www.peeringdb.com/fac/$id

            Example: 103
        """)

    organisation_name = serializers.CharField(
        max_length=80,
        source="operator_name",
        help_text="""
            Name of Datacenter operator
            Example: Moon Datacenters
        """)

    cluster =serializers.CharField(
        allow_null=True,
        allow_blank=True,
        required=False,
        max_length=80,
        source="cluster_name",
        help_text="""
            Group of facilities within the same building/campus
            Example: "60 Hudson, NYC"
        """)


class DeviceCapabilitySerializer(serializers.Serializer):
    """Device Capability"""
    media_type = serializers.CharField(
        read_only=True,
        max_length=20,
        help_text="""
            The media type of the port (e.g. 1000BASE-LX, 10GBASE-LR, ...)

            Example: "1000BASE-LX"
        """)
    speed = serializers.IntegerField(
        read_only=True,
        help_text="""
            Speed of port in Mbit/s

            Example: 1000
        """)

    qinq_capable = serializers.BooleanField(
        read_only=True,
        source="q_in_q",
        help_text="""
            Ports supports inner vlan tag (QinQ)

            Example: true
        """)
    max_lag = serializers.IntegerField(
        read_only=True,
        max_value=32767,
        min_value=0,
        help_text="""
            Maximum count of ports which can be bundled to a max_lag
            Example: 8
        """)
    availability = serializers.IntegerField(
        read_only=True,
        source="availability_count",
        max_value=2147483647,
        min_value=0,
        help_text="""
            Count of available ports on device

            Example: 23
        """)


class DeviceSerializerBase(serializers.Serializer):
    """Device Base"""
    id = serializers.CharField(read_only=True)

    name = serializers.CharField(
        max_length=180,
        help_text="""
            Name of the device

            Example: edge2.moon.space-ix.net
        """)

    capabilities = DeviceCapabilitySerializer(
        many=True, read_only=True)


class DeviceSerializer(DeviceSerializerBase):
    """Device"""
    physical_facility = JeaPrimaryKeyRelatedField(
        source="physical_facility_id",
        read_only=True,
        help_text="""
            Identifier of the facilitiy where the device
            is physically based.
        """)


class DeviceConnectionSerializer(serializers.Serializer):
    """Device Connection"""
    id = serializers.CharField(read_only=True)

    max_capacity = serializers.IntegerField(
        max_value=2147483647, min_value=0)
    device = serializers.PrimaryKeyRelatedField(
        queryset=Device.objects.all())
    connected_device = serializers.PrimaryKeyRelatedField(
        queryset=Device.objects.all())


class PointOfPresenceDeviceSerializer(serializers.Serializer):
    """Device with `presence` annotation"""
    id = serializers.CharField(read_only=True)
    presence = serializers.CharField(read_only=True)


class PointOfPresenceSerializer(serializers.Serializer):
    """Point Of Presence"""
    id = serializers.CharField(read_only=True)

    name = serializers.CharField(max_length=40)
    physical_facility = JeaPrimaryKeyRelatedField(
        read_only=True)

    reachable_facilities = serializers.ManyRelatedField(
        child_relation=JeaPrimaryKeyRelatedField(
            queryset=Facility.objects.all()))

    available_devices = JeaPrimaryKeyRelatedField(
        source="available_physical_devices",
        queryset=catalog_models.Device.objects.all(),
        many=True)


class ProductBaseSerializer(serializers.Serializer):
    """Product Base"""
    id = serializers.CharField(read_only=True)

    name = serializers.CharField(
        max_length=80,
        help_text="""
            Name of the product
        """)


    # available_devices = serializers.ManyRelatedField(
    #    source="devices",
    #    allow_empty=False,
    #    child_relation=serializers.PrimaryKeyRelatedField(
    #        allow_empty=False,
    #        queryset=Device.objects.all()))


class ExchangeLanNetworkProductSerializer(ProductBaseSerializer):
    """Exchange Lan Network Product"""
    metro_area = serializers.CharField(
        allow_blank=True,
        allow_null=True,
        max_length=3,
        required=False,
        help_text="""
            The metro area the product is available

            Example: MUC
        """)

    __polymorphic_type__ = catalog_models.PRODUCT_TYPE_EXCHANGE_LAN

class ClosedUserGroupNetworkProductSerializer(ProductBaseSerializer):
    """Closed User Group Network Product"""
    __polymorphic_type__ = catalog_models.PRODUCT_TYPE_CLOSED_USER_GROUP


class ELineNetworkProductSerializer(ProductBaseSerializer):
    """E-Line Network Product"""
    __polymorphic_type__ = catalog_models.PRODUCT_TYPE_ELINE


class CloudNetworkProductSerializer(ProductBaseSerializer):
    """Cloud Network Product"""
    zone = serializers.CharField(max_length=80)
    handover_point = serializers.CharField(max_length=80)

    provider = serializers.CharField(source="cloud_provider_name")

    __polymorphic_type__ = catalog_models.PRODUCT_TYPE_CLOUD


class ProductSerializer(PolymorphicSerializer):
    """Polymorphic Product"""
    serializer_classes = {
        catalog_models.PRODUCT_TYPE_EXCHANGE_LAN:
            ExchangeLanNetworkProductSerializer,
    }
    """
    catalog_models.PRODUCT_TYPE_CLOSED_USER_GROUP:
        ClosedUserGroupNetworkProductSerializer,
    catalog_models.PRODUCT_TYPE_ELINE:
        ELineNetworkProductSerializer,
    catalog_models.PRODUCT_TYPE_CLOUD:
        CloudNetworkProductSerializer,
    """

    entity_types = PRODUCT_TYPES
