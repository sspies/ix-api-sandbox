
"""
MAC Addresses Services
----------------------

Manage mac addresses. Schedule mac changes and
get all mac addresses relevant for the customer.
"""

from django.core.exceptions import ValidationError
from django.utils import timezone

from jea.exceptions import ResourceAccessDenied
from jea.ipam.models import MacAddress
from jea.ipam.filters import MacAddressFilter
from jea.ipam.events import (
    mac_address_created,
    mac_address_assigned,
    mac_address_removed,
)
from jea.access import models as access_models
from jea.access.services import configs as configs_svc
from jea.crm.services import customers as customers_svc
from jea.eventmachine import active


def get_mac_addresses(
        scoping_customer=None,
        filters=None,
    ):
    """
    Get all related mac addresses for the customer.

    :param scoping_customer: The customer mananging the address
    :param filters: A mapping of filter keys to values.
    """
    if scoping_customer:
        macs = scoping_customer.scoped_macaddresss.all()
    else:
        macs = MacAddress.objects.all()

    # And now: Filter these mac addresses
    filtered = MacAddressFilter(
        filters, queryset=macs)

    return filtered.qs


def get_mac_address(
        scoping_customer=None,
        mac_address=None,
    ):
    """
    Retrieve a single mac address. Perform ownership test.
    """
    # Get mac address from managed customer queryset, if
    # present.
    if not isinstance(mac_address, MacAddress):
        macs = get_mac_addresses(
            scoping_customer=scoping_customer)
        mac_address = macs.get(pk=mac_address)

    # Permission check
    if scoping_customer and \
        mac_address.scoping_customer_id != scoping_customer.pk:
        raise ResourceAccessDenied(mac_address)

    return mac_address


@active.command
def create_mac_address(
        dispatch,
        scoping_customer=None,
        mac_address_input=None,
    ) -> MacAddress:
    """
    Create a mac address object.
    """
    if not scoping_customer:
        raise ValidationError("A managing customer is required.")

    if not mac_address_input.get("managing_customer"):
        raise ValidationError("A billing customer is required.")

    if not mac_address_input.get("consuming_customer"):
        raise ValidationError("An owning customer is required.")

    # Fetch related customers and check permissions
    managing_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=mac_address_input["managing_customer"])

    consuming_customer = customers_svc.get_customer(
        scoping_customer=scoping_customer,
        customer=mac_address_input["consuming_customer"])

    # Check date constraints
    valid_not_before = mac_address_input.get("valid_not_before")
    valid_not_after = mac_address_input.get("valid_not_after")

    # We don't allow creating things in the past
    if not valid_not_before or valid_not_before < timezone.now():
        valid_not_before = timezone.now()

    # Make new MAC address
    mac_address = MacAddress(
        scoping_customer=scoping_customer,
        managing_customer=managing_customer,
        consuming_customer=consuming_customer,
        external_ref=mac_address_input["external_ref"],
        address=mac_address_input["address"],
        valid_not_before=valid_not_before,
        valid_not_after=valid_not_after)
    mac_address.save()

    # Trigger state transitions
    dispatch(mac_address_created(mac_address))
    mac_address.refresh_from_db()

    return mac_address


@active.command
def assign_mac_address(
        dispatch,
        scoping_customer=None,
        mac_address_input=None,
    ):
    """
    Assign a mac address.
    Right now the supported target is a network service.
    In the future others might be required aswell.
    """
    # Check ownership of related service
    config = configs_svc.get_network_service_config(
        scoping_customer=scoping_customer,
        network_service_config=mac_address_input["network_service_config"])

    # Load related customers
    managing_customer = mac_address_input.get("managing_customer")
    consuming_customer = mac_address_input.get("consuming_customer")
    if not managing_customer:
        managing_customer = config.managing_customer
    if not consuming_customer:
        consuming_customer = config.consuming_customer
    managing_customer = customers_svc.get_customer(
        customer=managing_customer,
        scoping_customer=scoping_customer)
    consuming_customer = customers_svc.get_customer(
        customer=consuming_customer,
        scoping_customer=scoping_customer)

    mac_address = mac_address_input.get("mac_address")
    if mac_address:
        # Ownership test
        mac_address = get_mac_address(
            scoping_customer=scoping_customer,
            mac_address=mac_address)

    if not mac_address:
        # Create a fresh mac from input. Or try to.
        valid_not_before = mac_address_input.get("valid_not_before")
        valid_not_after = mac_address_input.get("valid_not_after")
        if not valid_not_before or valid_not_before < timezone.now():
            valid_not_before = timezone.now()

        mac_address = MacAddress(
            address=mac_address_input["address"],
            valid_not_before=mac_address_input["valid_not_before"],
            valid_not_after=mac_address_input["valid_not_after"],
            scoping_customer=scoping_customer,
            managing_customer=managing_customer,
            consuming_customer=consuming_customer)
        mac_address.save()


    # Assign service by type
    if isinstance(config, access_models.ExchangeLanNetworkServiceConfig):
        mac_address.exchange_lan_network_service_configs.add(config) 
    elif isinstance(config, access_models.ClosedUserGroupNetworkServiceConfig):
        mac_address.closed_user_group_network_service_configs.add(config)
    else:
        raise ValidationError("Invalid network service.")

    # Trigger state progressions
    dispatch(mac_address_assigned(mac_address))
    mac_address.refresh_from_db()

    return mac_address


@active.command
def remove_mac_address(
        dispatch,
        scoping_customer=None,
        mac_address=None,
    ):
    """
    Remove a mac address from the network.

    :param managing_customer: The customer responisble for related object.
    :param consuming_customer: The owning customer of the related object.
    :param mac_address: A mac address object or an identifier.
    """
    mac_address = get_mac_address(
        scoping_customer=scoping_customer,
        mac_address=mac_address)

    # Check if mac address is in use
    if mac_address.exchange_lan_network_service_configs.all() or \
        mac_address.closed_user_group_network_service_configs.all():
        raise ValidationError("MAC address is currently in use")

    dispatch(mac_address_removed(mac_address))
    mac_address.delete()

    return mac_address

