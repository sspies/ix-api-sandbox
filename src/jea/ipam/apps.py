

import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)


class IpamConfig(AppConfig):
    name = "jea.ipam"

    def ready(self):
        """Application is ready"""
        logger.info("Initializing app: {}".format(self.name))
