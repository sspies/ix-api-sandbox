
"""
JEA Exceptions
--------------

Define common exceptions, all modules can use
"""

import inspect

from rest_framework import (
    status,
    exceptions as rf_exceptions,
)
from django.db import models as django_models


class ResourceAccessDenied(rf_exceptions.PermissionDenied):
    """
    Custom permission denied error, including the resource
    where the error occured.
    """
    def __init__(self, resource, pk=None):
        """Permission denied for resource"""
        if isinstance(resource, django_models.Model):
            resource_name = type(resource).__name__
            pk = resource.pk
        elif inspect.isclass(resource) and \
            issubclass(resource, django_models.Model):
            resource_name = resource.__name__
        else:
            resource_name = resource

        message = "You do not have permission to access '{}' with id={}".format(
            resource_name, pk)
        super().__init__(message)


class ConditionAssertionError(rf_exceptions.APIException):
    """
    We can not fulfill the request, because the
    current state is in conflict with the desired next state.
    """
    status_code = status.HTTP_409_CONFLICT
    default_code = "assertion_error"
    default_detail = "The request could not be fulfilled."


class ValidationError(rf_exceptions.ValidationError):
    """
    An extended validation error including the field
    which caused the exception.
    """
    default_field = None

    def __init__(self, detail=None, code=None, field=None):
        """Initialize the exception"""
        super().__init__(detail=detail, code=code)
        if not field:
            field = self.default_field
        self.field = field

