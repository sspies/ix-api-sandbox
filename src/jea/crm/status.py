
"""
Customer and Contact Status Messages
"""

from jea.eventmachine.models import (
    StatusMessage, Severity
)

NOC_CONTACT_MISSING = "noc_contact_missing"
LEGAL_CONTACT_MISSING = "legal_contact_missing"


def customer_contact_missing(customer, contact_type):
    """Create status message"""
    if contact_type == "noc":
        tag = NOC_CONTACT_MISSING
        message = "The customer requires a 'noc' contact"
        contact_type = "noc"
    else:
        tag = LEGAL_CONTACT_MISSING
        message = "The customer requires a 'legal' contact"
        contact_type = "legal"

    return StatusMessage(
        ref=customer,
        severity=Severity.CRITICAL, # Blocker
        message=message,
        attrs={
            "contact_type": contact_type,
        },
        tag=tag,
    )
