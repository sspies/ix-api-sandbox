# Generated by Django 2.1.5 on 2019-08-13 16:32

from django.db import migrations, models
import django.db.models.deletion
import enumfields.fields
import jea.eventmachine.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('purchase_order', models.CharField(max_length=80)),
                ('contract_ref', models.CharField(max_length=128, null=True)),
                ('external_ref', models.CharField(max_length=128, null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('state', enumfields.fields.EnumField(default='requested', enum=jea.eventmachine.models.State, max_length=255)),
                ('name', models.CharField(max_length=80)),
                ('external_ref', models.CharField(max_length=128, null=True)),
                ('parent', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='subcustomers', to='crm.Customer')),
                ('scoping_customer', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='scoped_customers', to='crm.Customer')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BillingContact',
            fields=[
                ('contact_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='crm.Contact')),
                ('legal_company_name', models.CharField(max_length=80)),
                ('address_country', models.CharField(max_length=2)),
                ('address_locality', models.CharField(max_length=80)),
                ('address_region', models.CharField(max_length=80, null=True)),
                ('postal_code', models.CharField(max_length=80)),
                ('post_office_box_number', models.CharField(max_length=80, null=True)),
                ('street_address', models.CharField(max_length=80)),
                ('email', models.CharField(max_length=80)),
                ('vat_number', models.CharField(max_length=20, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('crm.contact',),
        ),
        migrations.CreateModel(
            name='ImplementationContact',
            fields=[
                ('contact_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='crm.Contact')),
                ('name', models.CharField(max_length=80)),
                ('telephone', models.CharField(max_length=80, null=True)),
                ('email', models.CharField(max_length=80)),
                ('legal_company_name', models.CharField(max_length=80, null=True)),
            ],
            options={
                'abstract': False,
            },
            bases=('crm.contact',),
        ),
        migrations.CreateModel(
            name='LegalContact',
            fields=[
                ('contact_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='crm.Contact')),
                ('legal_company_name', models.CharField(max_length=80)),
                ('email', models.CharField(max_length=80)),
                ('address_country', models.CharField(max_length=2)),
                ('address_locality', models.CharField(max_length=80)),
                ('address_region', models.CharField(max_length=80, null=True)),
                ('postal_code', models.CharField(max_length=80)),
                ('post_office_box_number', models.CharField(max_length=80, null=True)),
                ('street_address', models.CharField(max_length=80)),
            ],
            options={
                'abstract': False,
            },
            bases=('crm.contact',),
        ),
        migrations.CreateModel(
            name='NocContact',
            fields=[
                ('contact_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='crm.Contact')),
                ('telephone', models.CharField(max_length=40, null=True)),
                ('email', models.CharField(max_length=80)),
            ],
            options={
                'abstract': False,
            },
            bases=('crm.contact',),
        ),
        migrations.CreateModel(
            name='PeeringContact',
            fields=[
                ('contact_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='crm.Contact')),
                ('email', models.CharField(max_length=80)),
            ],
            options={
                'abstract': False,
            },
            bases=('crm.contact',),
        ),
        migrations.AddField(
            model_name='contact',
            name='consuming_customer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='consumed_contacts', to='crm.Customer'),
        ),
        migrations.AddField(
            model_name='contact',
            name='managing_customer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='managed_contacts', to='crm.Customer'),
        ),
        migrations.AddField(
            model_name='contact',
            name='polymorphic_ctype',
            field=models.ForeignKey(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='polymorphic_crm.contact_set+', to='contenttypes.ContentType'),
        ),
        migrations.AddField(
            model_name='contact',
            name='scoping_customer',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, related_name='scoped_contacts', to='crm.Customer'),
        ),
    ]
