
"""
Test CRM filters
"""

import pytest
from model_mommy import mommy

from jea.crm import models, filters


@pytest.mark.django_db
def test_customer_filter__fields():
    """Test customer filtering by fields"""
    c1 = mommy.make("crm.Customer", _fill_optional=True)
    c2 = mommy.make("crm.Customer",
                    external_ref=c1.external_ref,
                    _fill_optional=True)
    c3 = mommy.make("crm.Customer",
                    _fill_optional=True)


    filtered = filters.CustomerFilter({
        "external_ref": str(c1.external_ref)
    })
    queryset = filtered.qs

    assert c1 in queryset
    assert c2 in queryset
    assert not c3 in queryset


@pytest.mark.django_db
def test_customer_filter__state():
    """Test customer filtering by state"""
    c1 = mommy.make("crm.Customer", state="production")
    c2 = mommy.make("crm.Customer", state="testing")

    filtered = filters.CustomerFilter({
        "state": "production",
    })
    queryset = filtered.qs

    assert c1 in queryset
    assert not c2 in queryset


@pytest.mark.django_db
def test_customer_filter__bulk_id():
    """Test customer filtering by primary key"""
    c1 = mommy.make("crm.Customer")
    c2 = mommy.make("crm.Customer")
    c3 = mommy.make("crm.Customer")

    filtered = filters.CustomerFilter({
        "id": f"{c1.pk},{c3.pk}",
    })
    queryset = filtered.qs

    assert c1 in queryset
    assert not c2 in queryset

    assert c3 in queryset


@pytest.mark.django_db
def test_contact_filter__customer():
    """Filter contact with customer"""
    c1 = mommy.make("crm.Customer")
    c2 = mommy.make("crm.Customer")

    ct1 = mommy.make("crm.Contact", consuming_customer=c1)
    ct2 = mommy.make("crm.Contact", consuming_customer=c1)
    ct3 = mommy.make("crm.Contact", consuming_customer=c2)

    filtered = filters.ContactFilter({
        "consuming_customer": str(c1.pk) ,
    })
    queryset = filtered.qs

    assert ct1 in queryset
    assert ct2 in queryset
    assert not ct3 in queryset


@pytest.mark.django_db
def test_contact_filter_polymorphic_type():
    """Test polymorphic type filtering"""
    c1 = mommy.make("crm.ImplementationContact")
    c2 = mommy.make("crm.NocContact")
    c3 = mommy.make("crm.LegalContact")
    c4 = mommy.make("crm.BillingContact")

    # Implementation
    filtered = filters.ContactFilter({
        "type": "implementation",
    })
    queryset = filtered.qs

    assert c1 in queryset
    assert not c2 in queryset
    assert not c3 in queryset
    assert not c4 in queryset

    # Noc 
    filtered = filters.ContactFilter({
        "type": "noc",
    })
    queryset = filtered.qs

    assert not c1 in queryset
    assert c2 in queryset
    assert not c3 in queryset
    assert not c4 in queryset

    # Legal
    filtered = filters.ContactFilter({
        "type": "legal",
    })
    queryset = filtered.qs

    assert not c1 in queryset
    assert not c2 in queryset
    assert c3 in queryset
    assert not c4 in queryset

    # Legal
    filtered = filters.ContactFilter({
        "type": "billing",
    })
    queryset = filtered.qs

    assert not c1 in queryset
    assert not c2 in queryset
    assert not c3 in queryset
    assert c4 in queryset

#
# Filter Mixins
#
@pytest.mark.django_db
def test_ownable_filter_mixin():
    """Test filter mixin with billing and owning customer"""
    customer_a = mommy.make("crm.Customer")
    customer_b = mommy.make("crm.Customer")
    c1 = mommy.make(
        "crm.LegalContact",
        managing_customer=customer_a,
        consuming_customer=customer_b)
    c2 = mommy.make(
        "crm.LegalContact",
        managing_customer=customer_b,
        consuming_customer=customer_a)

    # First filter by owning customer
    filtered = filters.OwnableFilterMixin(
        {"managing_customer": customer_a.pk},
        queryset=models.Contact.objects.all())
    assert c1 in filtered.qs
    assert not c2 in filtered.qs

    filtered = filters.OwnableFilterMixin(
        {"consuming_customer": customer_a.pk},
        queryset=models.Contact.objects.all())
    assert not c1 in filtered.qs
    assert c2 in filtered.qs

