import pytest
from model_mommy import mommy

from jea.crm.models import (
    Customer,
    LegalContact,
    NocContact,
    PeeringContact,
    ImplementationContact,
    BillingContact,
)


def test_customer_string_representation():
    """Customers should be representable in a string"""
    # Create a customer with fixed id and name
    customer = Customer(name="customer", id=42)

    assert str(customer), "Customer to string should be implemented"


@pytest.mark.django_db
def test_contact_model_polymorphism():
    """Contacts should have polymorphic behaviour"""
    # Create a customer and some contacts
    customer = mommy.make(Customer)

    created_contacts = [
        # Create some contacts:
        # - Legal
        mommy.make(LegalContact, consuming_customer=customer),
        # - NOC
        mommy.make(NocContact, consuming_customer=customer),
        # - Peering
        mommy.make(PeeringContact, consuming_customer=customer),
        # - Billing
        mommy.make(BillingContact, consuming_customer=customer),
        # - another billing contact
        mommy.make(BillingContact, consuming_customer=customer),
        # - Implementation
        mommy.make(ImplementationContact, consuming_customer=customer),
    ]

    # The customer should now have contacts:
    contacts = customer.consumed_contacts.all()
    assert len(contacts) == len(created_contacts)

    # These should be filterable by type:
    billing_contacts = customer.consumed_contacts.instance_of(BillingContact)

    assert len(billing_contacts) == 2


def test_contact__str__():
    """Test contact string representation"""
    contact = mommy.prepare("crm.Contact")
    assert str(contact)


def test_contact__repr__():
    """Test contact repr"""
    contact = mommy.prepare("crm.Contact")
    assert repr(contact)


def test_legal_contact__str__():
    """Test legal contact to string"""
    contact = mommy.prepare("crm.LegalContact")
    assert str(contact)


def test_legal_contact__repr__():
    """Test legal contact repr"""
    contact = mommy.prepare("crm.LegalContact")
    assert repr(contact)


def test_noc_contact__str__():
    """Test NOC contact to string"""
    contact = mommy.prepare("crm.NocContact")
    assert str(contact)


def test_noc_contact__repr__():
    """Test NOC contact to representation"""
    contact = mommy.prepare("crm.NocContact")
    assert repr(contact)


def test_peering_contact__repr__():
    """Test peering contact representation""" 
    contact = mommy.prepare("crm.PeeringContact")
    assert repr(contact)


def test_peering_contact__str__():
    """Test peering contact representation""" 
    contact = mommy.prepare("crm.PeeringContact")
    assert str(contact)


def test_billing_contact__str__():
    """Test Billing contact to string"""
    contact = mommy.prepare("crm.BillingContact")
    assert str(contact)


def test_billing_contact__repr__():
    """Test Billing Contact to representation"""
    contact = mommy.prepare("crm.BillingContact")
    assert repr(contact)

