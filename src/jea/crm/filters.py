
"""
Customer and Contact filtering based on django filters.
"""

import django_filters
from django.db import models

from utils import filters
from jea.crm import models as crm_models
from jea.crm.models import (
    Customer,
    Contact,
)


class OwnableFilterMixin(django_filters.FilterSet):
    """
    Define all filters for ownable objects as defined
    in `jea.crm.models.OwnableMixin`.
    """
    managing_customer = django_filters.CharFilter(
        field_name="managing_customer")
    consuming_customer = django_filters.CharFilter(
        field_name="consuming_customer")

    purchase_order = django_filters.CharFilter(
        field_name="purchase_order",
        lookup_expr="iexact")
    contract_ref = django_filters.CharFilter(
        field_name="contract_ref",
        lookup_expr="iexact")



class CustomerFilter(django_filters.FilterSet):
    """Filter customers"""
    id = filters.BulkIdFilter()

    # State
    state = django_filters.CharFilter(
        field_name="state", lookup_expr="iexact")

    state__is_not = django_filters.CharFilter(
        field_name="state", lookup_expr="iexact", exclude=True)


    class Meta:
        model = Customer
        fields = [
            "parent",
            "external_ref",
            "name",
        ]

        filter_overrides = {
            models.CharField: {
                'filter_class':
                    django_filters.CharFilter,
                'extra': lambda f: {
                    'lookup_expr': 'iexact',
                },
            },
        }


class ContactFilter(OwnableFilterMixin, django_filters.FilterSet):
    """Filter contacts"""
    TYPE_CHOICES = (
        (crm_models.CONTACT_TYPE_LEGAL,
         crm_models.CONTACT_TYPE_LEGAL),
        (crm_models.CONTACT_TYPE_NOC,
         crm_models.CONTACT_TYPE_NOC),
        (crm_models.CONTACT_TYPE_IMPLEMENTATION,
         crm_models.CONTACT_TYPE_IMPLEMENTATION),
        (crm_models.CONTACT_TYPE_BILLING,
         crm_models.CONTACT_TYPE_BILLING),
        (crm_models.CONTACT_TYPE_PEERING,
         crm_models.CONTACT_TYPE_PEERING),
    )

    # Field filters
    id = filters.BulkIdFilter()

    # Polymorphic Type Filter
    type = django_filters.ChoiceFilter(
        method="filter_type",
        choices=TYPE_CHOICES)

    def filter_type(self, queryset, _name, value):
        """Filter polymorphic by type"""
        contact_class = crm_models.INV_CONTACT_TYPES.get(value)
        if not contact_class:
            return queryset.none()

        # Apply filter
        return queryset.instance_of(contact_class)


    class Meta:
        model = Contact
        fields = [
            "external_ref",
        ]

