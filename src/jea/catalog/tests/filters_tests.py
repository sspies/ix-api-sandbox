
"""
Test Model Filters
"""

import pytest
from model_mommy import mommy

from jea.catalog.filters import (
    FacilityFilter, DeviceFilter, ProductFilter,
)
from jea.catalog.models import (
    Facility
)


@pytest.mark.django_db
def test_facility_filter():
    """Test facility filtering"""
    cluster = mommy.make("catalog.FacilityCluster")
    facility_a = mommy.make("catalog.Facility")
    facility_b = mommy.make("catalog.Facility", cluster=cluster)
    pop = mommy.make("catalog.PointOfPresence",
                     physical_facility=facility_b)
    device = mommy.make("catalog.Device",
                        physical_point_of_presence=pop)

    filter_requests = [
        ({"metro_area": facility_b.metro_area}, facility_b),
        ({"address_country": facility_a.address_country}, facility_a),
        ({"address_locality": facility_a.address_locality}, facility_a),
        ({"postal_code": facility_a.postal_code}, facility_a),
        ({"cluster": facility_b.cluster.name}, facility_b),
        ({"device": str(device.id)}, facility_b),
    ]

    for request, expected_facility in filter_requests:
        filtered = FacilityFilter(request)
        assert expected_facility == filtered.qs.first(), \
            (filtered.qs.first(), expected_facility)


@pytest.mark.django_db
def test_devices_filter():
    """Test filtering the devices"""
    device_a = mommy.make("catalog.Device")
    device_b = mommy.make("catalog.Device")

    # filter by id
    filtered = DeviceFilter({"id": f"{device_b.pk},12345689"})
    assert device_b in filtered.qs
    assert not device_a in filtered.qs

    # filter product
    product = mommy.make("catalog.Product")
    filtered = DeviceFilter({"product": product.pk})
    assert device_a in filtered.qs


@pytest.mark.django_db
def test_product_filter():
    """Test product filtering"""
    product_a = mommy.make("catalog.ExchangeLanNetworkProduct")
    product_b = mommy.make("catalog.CloudNetworkProduct")

    filtered = ProductFilter({"type": "exchange_lan"})
    assert product_a in filtered.qs
    assert not product_b in filtered.qs

    # Filter device
    device = mommy.make("catalog.Device")
    filtered = ProductFilter({"device": device.pk})
    assert product_a in filtered.qs

