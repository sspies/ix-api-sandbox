
"""
Test Products Service
"""

import pytest
from model_mommy import mommy

from jea.catalog.services import (
    products as products_svc,
)

@pytest.mark.django_db
def test_get_products():
    """Just list all products"""
    product = mommy.make("catalog.ExchangeLanNetworkProduct")
    products = products_svc.get_products()

    assert product in products


@pytest.mark.django_db
def test_list_products_filter_type():
    """List products of a specific type"""
    exchange_lan_product = mommy.make("catalog.ExchangeLanNetworkProduct")
    closed_user_group_product = mommy.make("catalog.ClosedUserGroupNetworkProduct")
    eline_product = mommy.make("catalog.ELineNetworkProduct")
    cloud_product = mommy.make("catalog.CloudNetworkProduct")

    products = products_svc.get_products(filters={"type": "exchange_lan"})
    assert exchange_lan_product in products
    assert not closed_user_group_product in products
    assert not eline_product in products
    assert not cloud_product in products

    """
    products = products_svc.get_products(filters={"type": "closed_user_group"})
    assert not exchange_lan_product in products
    assert closed_user_group_product in products
    assert not eline_product in products
    assert not cloud_product in products

    products = products_svc.get_products(filters={"type": "eline"})
    assert not exchange_lan_product in products
    assert not closed_user_group_product in products
    assert eline_product in products
    assert not cloud_product in products

    products = products_svc.get_products(filters={"type": "cloud"})
    assert not exchange_lan_product in products
    assert not closed_user_group_product in products
    assert not eline_product in products
    assert cloud_product in products
    """


@pytest.mark.django_db
def test_list_products_filter_zone():
    """List products of a specific type"""
    exchange_lan_product = mommy.make("catalog.ExchangeLanNetworkProduct",
                                      metro_area="MTT")
    closed_user_group_product = mommy.make("catalog.ClosedUserGroupNetworkProduct")
    eline_product = mommy.make("catalog.ELineNetworkProduct")
    cloud_product = mommy.make("catalog.CloudNetworkProduct",
                               handover_point="hanover",
                               zone="foo-centraal-2")

    products = products_svc.get_products(filters={"zone": "foo-centraal-2"})
    assert not exchange_lan_product in products
    assert not closed_user_group_product in products
    assert not eline_product in products
    assert cloud_product in products

    products = products_svc.get_products(filters={
        "handover_point": "hanover"})
    assert not exchange_lan_product in products
    assert not closed_user_group_product in products
    assert not eline_product in products
    assert cloud_product in products

    products = products_svc.get_products(filters={"metro_area": "mtt"})
    assert exchange_lan_product in products
    assert not closed_user_group_product in products
    assert not eline_product in products
    assert not cloud_product in products

