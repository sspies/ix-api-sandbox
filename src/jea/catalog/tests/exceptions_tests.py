
"""
Test Catalog Application Exceptions
"""

from jea.catalog.exceptions import (
    MediaTypeUnavailable,
)


def test_media_type_unavailable():
    """Test exception"""
    exc = MediaTypeUnavailable()
    assert isinstance(exc, Exception)


