
"""
Test Catalog Models and Relations
"""

import pytest
from model_mommy import mommy

from jea.catalog.models import (
    Facility,
    FacilityCluster,
    FacilityOperator,
    Device,
    DeviceConnection,
    DeviceCapability,
    PointOfPresence,
    Product,
    ExchangeLanNetworkProduct,
    ClosedUserGroupNetworkProduct,
    ELineNetworkProduct,
    CloudNetworkProduct,
    MediaType,
)


@pytest.mark.django_db
def test_facilities():
    """Test Facilities and Facility Clusters"""
    operator = mommy.make(FacilityOperator)

    # Create some facility
    facility = mommy.make(Facility, operator=operator)
    assert facility.cluster == None
    assert facility.operator == operator

    # Make another facility
    mommy.make(Facility, operator=operator)

    # Find facilities operated by a specific operator
    facilities = operator.facilities.all()
    assert len(facilities) == 2


@pytest.mark.django_db
def test_facility_clusters():
    """Test facility clustering"""
    # Assign the facility to a cluster
    facility = mommy.make(Facility)
    cluster = mommy.make(FacilityCluster)
    facility.cluster = cluster
    facility.save()

    # Create another facility
    mommy.make(Facility, cluster=cluster)

    # Get all facilities in this cluster
    facilities = cluster.facilities.all()
    assert len(facilities) == 2


@pytest.mark.django_db
def test_facility_physical_devices():
    """Test getting all physical devices in a facility"""
    facility = mommy.make(Facility)
    pop = mommy.make(PointOfPresence, physical_facility=facility)
    dev_a = mommy.make(Device, physical_point_of_presence=pop)

    assert dev_a in facility.physical_devices

@pytest.mark.django_db
def test_facility_reachable_devices():
    """Test getting all physical devices in a facility"""
    facility = mommy.make(Facility)
    pop = mommy.make(PointOfPresence, physical_facility=facility)
    dev_b = mommy.make(Device, reachable_points_of_presence=[pop])

    assert dev_b in facility.reachable_devices

@pytest.mark.django_db
def test_facility_available_devices():
    """Test getting all physical devices in a facility"""
    facility = mommy.make(Facility)
    pop = mommy.make(PointOfPresence, physical_facility=facility)
    dev_a = mommy.make(Device, physical_point_of_presence=pop)
    dev_b = mommy.make(Device, reachable_points_of_presence=[pop])

    assert dev_a in facility.available_devices
    assert dev_b in facility.available_devices


@pytest.mark.django_db
def test_devices():
    """Test Basic Devices"""
    pop = mommy.make(PointOfPresence)

    # Make some devices
    mommy.make(Device, physical_point_of_presence=pop)
    mommy.make(Device, physical_point_of_presence=pop)

    # Find devices as physical devices in a facility
    devices = pop.physical_devices.all()
    assert len(devices) == 2


@pytest.mark.django_db
def test_devices_physical_facility():
    """Test devices physical facility via demarc property"""
    facility = mommy.make(Facility)
    pop = mommy.make(PointOfPresence, physical_facility=facility)
    device = mommy.make(Device, physical_point_of_presence=pop)

    assert device.physical_facility == facility


@pytest.mark.django_db
def test_device_connections():
    """Test device to device connections"""
    device_a = mommy.make(Device)
    device_b = mommy.make(Device)

    # Device a and b are connected with a symmetric connection
    connection_ab = mommy.make(
        DeviceConnection,
        device=device_a,
        connected_device=device_b,
        max_capacity=1000)

    connection_ba = mommy.make(
        DeviceConnection,
        device=device_b,
        connected_device=device_a,
        max_capacity=1000)

    # Check connections
    assert connection_ab in device_a.outbound_device_connections.all()
    assert connection_ab in device_b.inbound_device_connections.all()

    assert connection_ba in device_a.inbound_device_connections.all()
    assert connection_ba in device_b.outbound_device_connections.all()


@pytest.mark.django_db
def test_device_capabilities():
    """Test device capabilities"""
    capa = mommy.make(DeviceCapability, speed=1000)
    device = capa.device

    # Make more capabilities
    mommy.make(DeviceCapability,
               device=device,
               speed=400000)

    mommy.make(DeviceCapability,
               device=device,
               media_type=MediaType.TYPE_100GBASE_LR.value,
               speed=100000)

    # Check filters
    capabilities = device.capabilities.filter(speed=1000)
    assert capabilities.count() == 1

    capabilities = device.capabilities.filter(speed=400000)
    assert capabilities.count() == 1

    capabilities = device.capabilities.filter(
        speed=100000,
        media_type=MediaType.TYPE_100GBASE_LR.value)
    assert capabilities.count() == 1


@pytest.mark.django_db
def test_pop_facilities():
    """Test PointOfPresence model with facilities"""
    facility_a = mommy.make(Facility)
    facility_b = mommy.make(Facility)

    pop_1 = mommy.make(PointOfPresence,
                       physical_facility=facility_a)
    pop_2 = mommy.make(PointOfPresence,
                       physical_facility=facility_a)
    pop_3 = mommy.make(PointOfPresence,
                       physical_facility=facility_b)

    # Find demarcation points
    assert facility_a.physical_points_of_presence.all().count() == 2
    assert facility_b.physical_points_of_presence.all().count() == 1


@pytest.mark.django_db
def test_pop_devices():
    """Test PointOfPresence model with devices"""
    d_1 = mommy.make(Device)
    d_2 = mommy.make(Device)
    d_3 = mommy.make(Device)

    pop_1 = mommy.make(PointOfPresence,
                       physical_devices=[d_1, d_2])

    pop_2 = mommy.make(PointOfPresence,
                       physical_devices=[d_3])

    # Check related
    assert d_1.physical_point_of_presence == pop_1
    assert d_2.physical_point_of_presence == pop_1
    assert d_3.physical_point_of_presence == pop_2


@pytest.mark.django_db
def test_pop_reachable_devices():
    """Test reachable devices relation from PointOfPresence"""
    d_1 = mommy.make(Device)
    d_2 = mommy.make(Device)

    pop_1 = mommy.make(PointOfPresence,
                       physical_devices=[d_1],
                       reachable_devices=[d_2])

    assert d_1 in pop_1.physical_devices.all()
    assert d_2 in pop_1.reachable_devices.all()


@pytest.mark.django_db
def test_pops_available_devices():
    """Test get all devices the demarc is available at"""
    fac_root = mommy.make(Facility)
    pop_root = mommy.make(PointOfPresence,
                          physical_facility=fac_root)

    d_1 = mommy.make(Device)
    d_2 = mommy.make(Device, physical_point_of_presence=pop_root)

    pop_1 = mommy.make(PointOfPresence,
                       physical_devices=[d_1],
                       reachable_devices=[d_2])

    devices = pop_1.available_devices

    assert d_1 in devices
    assert d_2 in devices


@pytest.mark.django_db
def test_pops_available_devices_annotations():
    """Test get all devices the demarc is available at"""
    fac_root = mommy.make(Facility)
    pop_root = mommy.make(PointOfPresence, physical_facility=fac_root)

    d_1 = mommy.make(Device, name="device_a")
    d_2 = mommy.make(Device,
                     physical_point_of_presence=pop_root,
                     name="device_b")

    pop_1 = mommy.make(PointOfPresence,
                      physical_devices=[d_1],
                      reachable_devices=[d_2])

    devices = pop_1.available_devices.order_by("name")
    assert devices[0].presence == "physical"
    assert devices[1].presence == "remote"


@pytest.mark.django_db
def test_pops_reachable_facilities():
    """Test get all remote facilities the demarc is available at"""
    fac_a = mommy.make(Facility, name="facility_a")
    fac_b = mommy.make(Facility, name="facility_b")

    pop_1 = mommy.make(PointOfPresence, physical_facility=fac_a)
    pop_2 = mommy.make(PointOfPresence, physical_facility=fac_b)

    mommy.make(Device, physical_point_of_presence=pop_1)
    mommy.make(Device,
               physical_point_of_presence=pop_2,
               reachable_points_of_presence=[pop_1])

    # Get remote facilities
    facilities = pop_1.reachable_facilities

    assert fac_b in facilities
    assert not fac_a in facilities


@pytest.mark.django_db
def test_polymorphic_products():
    """Test products and derived polymorphic products"""
    p_1 = mommy.make(ExchangeLanNetworkProduct)
    p_2 = mommy.make(ClosedUserGroupNetworkProduct)
    p_3 = mommy.make(ELineNetworkProduct)
    p_4 = mommy.make(CloudNetworkProduct)

    product = Product.objects.get(pk=p_1.id)
    assert type(product) == ExchangeLanNetworkProduct

    product = Product.objects.get(pk=p_2.id)
    assert type(product) == ClosedUserGroupNetworkProduct


@pytest.mark.django_db
def test_cloud_network_product_provider():
    """Test product provider relation"""
    p_1 = mommy.make(ExchangeLanNetworkProduct)
    p_2 = mommy.make(ClosedUserGroupNetworkProduct)
    p_3 = mommy.make(CloudNetworkProduct)

    d_1 = mommy.make(Device, products=[p_1])
    d_2 = mommy.make(Device, products=[p_1, p_3])
    d_3 = mommy.make(Device, products=[p_1, p_2, p_3])

    # Check relations: Device -> Product
    assert d_1.products.count() == 1
    assert d_1.products.first() == p_1
    assert d_2.products.count() == 2
    assert d_3.products.count() == 3

    # Product -> Device
    assert set(p_1.devices.all()) == {d_1, d_2, d_3}
    assert p_3.devices.count() == 2 # 2, 3

