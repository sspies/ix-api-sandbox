"""
Facilities Service
------------------

Get and list (filtered) facilities.
"""

from typing import Optional

from django.db.models.query import QuerySet

from jea.eventmachine import active
from jea.catalog.filters import (
    FacilityFilter,
)
from jea.catalog.models import (
    Facility,
)


def get_facilities(scoping_customer=None, filters=None) -> QuerySet:
    """
    List all facilities. Filter the result set.

    :param scoping_customer: The current customer interacting
                             with the system.
    :param filters: A dict of filter parameters.
    """
    filtered = FacilityFilter(filters)
    return filtered.qs


def get_facility(scoping_customer=None, facility=None) -> Facility:
    """
    Get a facility.
    Implemented lookups are by: faclity_id.

    :param scoping_customer: The current customer interacting
                             with the system.
    :param facilty: The id of the facility to access or
                    a facility object.
    """
    if isinstance(facility, Facility):
        return facility

    return Facility.objects.get(pk=facility)

