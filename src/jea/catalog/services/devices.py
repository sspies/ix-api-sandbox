
"""
Manage devices
"""

from typing import Optional

from django.db.models.query import QuerySet

from jea.catalog.models import (
    Device,
)
from jea.catalog.filters import (
    DeviceFilter,
)


def get_devices(
        scoping_customer=None,
        filters=None,
    ) -> QuerySet:
    """
    List all devices and apply filters.

    :param filters: A dict of filters
    """
    filtered = DeviceFilter(filters)
    return filtered.qs


def get_device(
        scoping_customer=None,
        device=None,
    ) -> Device:
    """
    Get a single device.

    Implemented lookups:
      - device_id

    :param device: If given, identify the device by the provided id.
    """
    if isinstance(device, Device):
        return device # Nothing to do here.

    return Device.objects.get(pk=device)

