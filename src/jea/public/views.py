
"""
JEA :: Sandbox Public Interface

While this is most likely only useful in development,
it is nice to provide some root level entry page to the project.
"""

from django.shortcuts import render


def index_page(request):
    """Render welcome / entry / root page"""
    return render(request, "jea/index/index.html")

