

"""
Test event creators
"""

import pytest
from model_mommy import mommy

from jea.eventmachine.models import (
    Event,
    State,
)
from jea.access import events

@pytest.mark.django_db
def test_demarc_requested():
    """Test port demarcation request event"""
    demarc = mommy.make("access.DemarcationPoint")
    event = events.demarc_requested(demarc)

    assert isinstance(event, Event)


@pytest.mark.django_db
def test_demarc_allocated():
    """Demarcation Point Allocated Event"""
    demarc = mommy.make("access.DemarcationPoint")
    event = events.demarc_allocated(demarc)

    assert isinstance(event, Event)


@pytest.mark.django_db
def test_demarc_released():
    """Test port demarc released"""
    demarc = mommy.make("access.DemarcationPoint")
    demarc.delete()

    event = events.demarc_released(demarc)
    event.save()

    assert isinstance(event, Event)

@pytest.mark.django_db
def test_demarc_connected():
    """Test connect event of port demarcs"""
    demarc = mommy.make("access.DemarcationPoint")
    connection = mommy.make("access.Connection")
    event = events.demarc_connected(demarc, connection)
    event.save()

    assert isinstance(event, Event)


@pytest.mark.django_db
def test_demarc_disconnected():
    """Test disconnect event"""
    demarc = mommy.make("access.DemarcationPoint")
    connection = mommy.make("access.Connection")
    event = events.demarc_disconnected(demarc, connection)
    event.save()

    assert isinstance(event, Event)


@pytest.mark.django_db
def test_demarc_updated():
    """Test demarc updated event creator"""
    demarc = mommy.make("access.DemarcationPoint")
    event = events.demarc_updated(demarc)
    event.save()

    assert isinstance(event, Event)


@pytest.mark.django_db
def test_demarc_state_changed():
    """Test state changed event"""
    demarc = mommy.make("access.DemarcationPoint")
    event = events.demarc_state_changed(
        demarc,
        prev_state=State.REQUESTED,
        next_state=State.ALLOCATED)

    event.save()

@pytest.mark.django_db
def test_demarc_state_changed_infered():
    """Test change state with infered next state"""
    demarc = mommy.make(
        "access.DemarcationPoint",
        state=State.PRODUCTION)
    event = events.demarc_state_changed(
        demarc,
        prev_state=State.ALLOCATED)

    assert event.payload["prev_state"] == "allocated"
    assert event.payload["next_state"] == "production"


@pytest.mark.django_db
def test_connection_created():
    """Test connection created"""
    connection = mommy.make("access.Connection")
    event = events.connection_created(connection)

    event.save()


@pytest.mark.django_db
def test_connection_destroyed():
    """Test connection created"""
    connection = mommy.make("access.Connection")
    event = events.connection_destroyed(connection)

    event.save()


@pytest.mark.django_db
def test_connection_state_changed():
    """Test state change of connection"""
    connection = mommy.make("access.Connection", state=State.PRODUCTION)

    event = events.connection_state_changed(connection,
                                            prev_state=State.ALLOCATED)
    event.save()
    assert event.payload["prev_state"] == "allocated"
    assert event.payload["next_state"] == "production"

    # Check next_state override
    event = events.connection_state_changed(connection,
                                            next_state=State.DECOMMISSIONED,
                                            prev_state=State.PRODUCTION)
    event.save()

    assert event.payload["prev_state"] == "production"
    assert event.payload["next_state"] == "decommissioned"


@pytest.mark.django_db
def test_network_service_config_created():
    """Test service config created event"""
    config = mommy.make("access.ExchangeLanNetworkServiceConfig")
    event = events.network_service_config_created(config)
    event.save()


@pytest.mark.django_db
def test_network_service_config_update():
    """Test service config was updated event"""
    config = mommy.make("access.ClosedUserGroupNetworkServiceConfig")
    event = events.network_service_config_updated(config)
    event.save()

@pytest.mark.django_db
def test_network_service_config_destroyed():
    """Test service config destruction event"""
    config = mommy.make("access.ClosedUserGroupNetworkServiceConfig")
    event = events.network_service_config_destroyed(config)
    event.save()


@pytest.mark.django_db
def test_network_service_config_state_changed():
    """Test state change event of network service config"""
    config = mommy.make("access.ELineNetworkServiceConfig",
        state=State.PRODUCTION)
    event = events.network_service_config_state_changed(
        config, prev_state=State.ALLOCATED)

    event.save()
    assert event.payload["prev_state"] == "allocated"
    assert event.payload["next_state"] == "production"


@pytest.mark.django_db
def test_network_feature_config_created():
    """Test feature config created event"""
    config = mommy.make("access.RouteServerNetworkFeatureConfig")
    event = events.network_feature_config_created(config)
    event.save()


@pytest.mark.django_db
def test_network_feature_config_updated():
    """Test feature config update event"""
    config = mommy.make("access.BlackholingNetworkFeatureConfig")
    event = events.network_feature_config_updated(config)
    event.save()


@pytest.mark.django_db
def test_network_feature_config_destroyed():
    """Test feature config destruction event"""
    config = mommy.make("access.BlackholingNetworkFeatureConfig")
    event = events.network_feature_config_destroyed(config)
    event.save()


@pytest.mark.django_db
def test_network_feature_config_state_changed():
    """Test feature config state change"""
    config = mommy.make("access.RouteServerNetworkFeatureConfig",
        state=State.PRODUCTION)
    event = events.network_feature_config_state_changed(
        config, prev_state=State.ALLOCATED)

    event.save()
    assert event.payload["prev_state"] == "allocated"
    assert event.payload["next_state"] == "production"


