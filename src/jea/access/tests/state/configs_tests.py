
"""
Test config state transitions
"""

import pytest
from model_mommy import mommy


from jea.access.state import configs as configs_stm


@pytest.mark.django_db
def test__next_exchange_lan_network_service_config_state():
    """Test next state calculation"""
    # Todo: this could be a bit better.
    service = mommy.make("service.ExchangeLanNetworkService")
    feature = mommy.make("service.RouteServerNetworkFeature",
        network_service=service,
        required=True)
    config = mommy.make("access.ExchangeLanNetworkServiceConfig",
        network_service=service)

    configs_stm._next_exchange_lan_network_service_config_state(
        lambda x: print(x),
        config)

    # Check status messages.
    # There should be at least one feature missing:
    status = config.status.all()
    assert status, "There should be at least one error"


@pytest.mark.django_db
def test_next_network_feature_config_state():
    """Test network feature config state"""
    config = mommy.make("access.RouteServerNetworkFeatureConfig")
    event = mommy.make("eventmachine.Event",
        ref=config)

    configs_stm.next_network_feature_config_state(event)


