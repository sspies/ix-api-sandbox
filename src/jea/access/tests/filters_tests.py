
"""
Test access model filters
"""

import pytest
from model_mommy import mommy

from jea.access import filters as access_filters


#
# Demarcs
#
@pytest.mark.django_db
def test_demarcation_point_filter():
    """Just test the filter instanciation"""
    d1 = mommy.make("access.DemarcationPoint")

    demarcs = access_filters.DemarcationPointFilter(None).qs
    assert d1 in demarcs


@pytest.mark.django_db
def test_demarcation_point_filter__state():
    """Test demarc filtering: state"""
    d1 = mommy.make(
        "access.DemarcationPoint",
        state="error")

    d2 = mommy.make(
        "access.DemarcationPoint",
        state="production")

    # Do some filtering:
    demarcs = access_filters.DemarcationPointFilter({
        "state": "error",
    }).qs

    assert d1 in demarcs
    assert not d2 in demarcs

    demarcs = access_filters.DemarcationPointFilter({
        "state__is_not": "error",
    }).qs

    assert not d1 in demarcs
    assert d2 in demarcs


@pytest.mark.django_db
def test_demarcation_point_filter__relations():
    """Test relations filtering"""
    conn = mommy.make("access.Connection")
    d1 = mommy.make("access.DemarcationPoint", connection=conn)
    d2 = mommy.make("access.DemarcationPoint")

    # Filter
    demarcs = access_filters.DemarcationPointFilter({
        "connection": str(conn.pk)
    }).qs

    assert d1 in demarcs
    assert not d2 in demarcs

#
# Connections
#
@pytest.mark.django_db
def test_connection_filter():
    """Test filter instanciation"""
    conn = mommy.make("access.Connection")
    connections = access_filters.ConnectionFilter().qs

    assert conn in connections


@pytest.mark.django_db
def test_connection_filter__state():
    """Test filtering connection by state"""
    conn1 = mommy.make("access.Connection", state="error")
    conn2 = mommy.make("access.Connection", state="production")

    connections = access_filters.ConnectionFilter({
        "state": "error",
    }).qs

    assert conn1 in connections
    assert not conn2 in connections

    connections = access_filters.ConnectionFilter({
        "state__is_not": "error",
    }).qs

    assert not conn1 in connections
    assert conn2 in connections


@pytest.mark.django_db
def test_connection_filter__demarc():
    """Test filtering connections by demarc"""
    demarc1 = mommy.make("access.DemarcationPoint")
    demarc2 = mommy.make("access.DemarcationPoint")

    conn1 = mommy.make("access.Connection",
        demarcation_points=[demarc1])

    conn2 = mommy.make("access.Connection",
        demarcation_points=[demarc2])

    # Filter one
    connections = access_filters.ConnectionFilter({
        "demarc": f"{demarc1.pk}",
    }).qs

    assert conn1 in connections
    assert not conn2 in connections

    # Filter many
    connections = access_filters.ConnectionFilter({
        "demarc": f"{demarc1.pk},{demarc2.pk}",
    }).qs

    assert conn1 in connections
    assert conn2 in connections


#
# Service Configs
#
@pytest.mark.django_db
def test_network_service_config_filter():
    """Test filter instance"""
    c1 = mommy.make("access.NetworkServiceConfig")
    configs = access_filters.NetworkServiceConfigFilter().qs

    assert c1 in configs


@pytest.mark.django_db
def test_network_service_config_filter__state():
    """Test filtering by state"""
    c1 = mommy.make("access.NetworkServiceConfig", state="error")
    c2 = mommy.make("access.NetworkServiceConfig", state="production")

    configs = access_filters.NetworkServiceConfigFilter({
        "state": "error",
    }).qs

    assert c1 in configs
    assert not c2 in configs

    configs = access_filters.NetworkServiceConfigFilter({
        "state__is_not": "error",
    }).qs

    assert not c1 in configs
    assert c2 in configs


@pytest.mark.django_db
def test_network_service_Config_filter__relations():
    """Test filtering on relations"""
    s1 = mommy.make("service.ExchangeLanNetworkService")
    conn1 = mommy.make("access.Connection")

    c1 = mommy.make("access.NetworkServiceConfig",
        network_service=s1)
    c2 = mommy.make("access.NetworkServiceConfig",
        connection=conn1)

    # Filter by service relation
    configs = access_filters.NetworkServiceConfigFilter({
        "service": s1.pk
    }).qs

    assert c1 in configs
    assert not c2 in configs

    # Filter by connection relation
    configs = access_filters.NetworkServiceConfigFilter({
        "connection": str(conn1.pk),
    }).qs

    assert not c1 in configs
    assert c2 in configs

#
# Feature Configs
#
@pytest.mark.django_db
def test_feature_config_filter():
    """Test filter instanciation"""
    c1 = mommy.make("access.RouteServerNetworkFeatureConfig")
    configs = access_filters.NetworkFeatureConfigFilter().qs
    assert c1 in configs


@pytest.mark.django_db
def test_feature_config_filter__state():
    """Test filtering by state"""
    c1 = mommy.make("access.RouteServerNetworkFeatureConfig",
                    state="error")
    c2 = mommy.make("access.BlackholingNetworkFeatureConfig",
                    state="production")

    configs = access_filters.NetworkFeatureConfigFilter({
        "state": "production",
    }).qs

    assert not c1 in configs
    assert c2 in configs

    configs = access_filters.NetworkFeatureConfigFilter({
        "state__is_not": "production",
    }).qs

    assert c1 in configs
    assert not c2 in configs


@pytest.mark.django_db
def test_feature_config_filter__type():
    """Test filtering by type"""
    c1 = mommy.make("access.RouteServerNetworkFeatureConfig")
    c2 = mommy.make("access.BlackholingNetworkFeatureConfig")
    c3 = mommy.make("access.IXPRouterNetworkFeatureConfig")

    # RS
    configs = access_filters.NetworkFeatureConfigFilter({
        "type": "route_server",
    }).qs
    assert c1 in configs
    assert not c2 in configs
    assert not c3 in configs

    """
    # Blackholing
    configs = access_filters.NetworkFeatureConfigFilter({
        "type": "blackholing",
    }).qs
    assert not c1 in configs
    assert c2 in configs
    assert not c3 in configs

    # IXPRouter
    configs = access_filters.NetworkFeatureConfigFilter({
        "type": "ixp_router",
    }).qs
    assert not c1 in configs
    assert not c2 in configs
    assert c3 in configs
    """


@pytest.mark.django_db
def test_feature_config_filter__relations():
    """Test filter by related keys"""
    service_config = mommy.make("access.ExchangeLanNetworkServiceConfig")
    feature = mommy.make("service.RouteServerNetworkFeature")

    c1 = mommy.make("access.RouteServerNetworkFeatureConfig",
                    network_feature=feature)
    c2 = mommy.make("access.IXPRouterNetworkFeatureConfig",
        network_service_config=service_config)

    # Filter feature relation
    configs = access_filters.NetworkFeatureConfigFilter({
        "network_feature": str(feature.pk),
    }).qs

    assert c1 in configs
    assert not c2 in configs

    # Filter by service configuration
    configs = access_filters.NetworkFeatureConfigFilter({
        "service_config": service_config.pk,
    }).qs

    assert not c1 in configs
    assert c2 in configs
