
import logging

from django.apps import AppConfig

logger = logging.getLogger(__name__)

class AccessConfig(AppConfig):
    name = 'jea.access'
    verbose_name = "JEA :: Access"

    def ready(self):
        """Application is ready"""
        logger.info("Initializing app: {}".format(self.name))

        # Register state change handlers
        from jea.access.state import demarcs
        from jea.access.state import connections
        from jea.access.state import configs
