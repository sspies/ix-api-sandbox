
"""
Demarcation Point Background Tasks
----------------------------------

These tasks are for more... waiting.
"""

import time

from django.conf import settings

from backend.celery import app as celery_app
from jea.eventmachine import active
from jea.eventmachine.models import State
from jea.access import models as access_models
from jea.access.events import (
    demarc_state_changed,
)


@celery_app.task
def set_demarc_state_task(demarc_id, next_state, wait):
    """
    After some waiting time, we set our port demarc to the
    next state.

    :param port_demarc_id: The port demarcation point identifier
    :param next_state: The demarc will assume this state after a waiting time.
    :param wait: Wait this amount of seconds before doing so.
    """
    # Wait for some time
    time.sleep(wait)

    demarc = access_models.DemarcationPoint.objects.get(pk=demarc_id)
    prev_state = demarc.state
    demarc.state = next_state
    demarc.save()

    dispatch = active.make_dispatch(None)
    dispatch(demarc_state_changed(demarc, prev_state))


def set_demarc_state(
        demarc: access_models.DemarcationPoint,
        next_state: State,
        wait: float = 2.5,
    ):
    """Spawn set connection state task"""
    if getattr(settings, "TESTING", False):
        print("Skipping task dispatch in testing.")
        return

    return set_demarc_state_task.delay(demarc.pk, next_state.value, wait)

