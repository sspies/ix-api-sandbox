
"""
Service Exceptions Tests
"""

from model_mommy import mommy

from jea.service import exceptions


def test_network_service_not_available():
    """Test network service not available exception"""
    service = mommy.prepare("service.ExchangeLanNetworkService")
    exc = exceptions.NetworkServiceNotAvailable(service)
    assert isinstance(exc.message, str)


def test_network_feature_not_available():
    """test network feature not available exception"""
    feature = mommy.prepare("service.BlackholingNetworkFeature")
    exc = exceptions.NetworkFeatureNotAvailable(feature)
    assert isinstance(exc.message, str)


def test_network_feature_not_available_with_service():
    """Test feature not available for a network service"""
    feature = mommy.prepare("service.BlackholingNetworkFeature")
    service = mommy.prepare("service.ExchangeLanNetworkService")

    exc = exceptions.NetworkFeatureNotAvailable(
        feature, network_service=service)

    assert isinstance(exc.message, str)

