
"""
JEA Service: Network

Access and manage all network services, like
Exchange Lans, Closed User Groups, etc...
"""

from typing import Optional

from django.core.exceptions import ValidationError
from django.db.models.query import QuerySet

from jea.eventmachine import active
from jea.service.models import (
    NetworkService,
    ExchangeLanNetworkService,
    NetworkFeature,
)
from jea.service.exceptions import (
    NetworkFeatureNotAvailable,
)
from jea.service.filters import (
    NetworkServiceFilter,
    NetworkFeatureFilter,
)


def get_network_services(
        scoping_customer=None,
        filters=None,
    ) -> QuerySet:
    """
    List all network services and apply filters

    :param _dispatch: A bound active dispatch for event emitting.
    :param filters: A dict of filters.
    """
    filtered = NetworkServiceFilter(filters)

    return filtered.qs


def get_network_service(
        network_service=None,
        scoping_customer=None,
    ) -> Optional[NetworkService]:
    """
    Get a specific network service.
    Right now the supported lookup methods are:
     - service_id

    TODO: Add billing and owning customer check for services
          which are customer instances. E.g. ClosedUserGroups

    :param service_id: The primary key of the network service
    """
    if not network_service:
        raise ValidationError("A service identifier or object is required")

    if isinstance(network_service, NetworkService):
        return network_service

    return NetworkService.objects.get(pk=network_service)



def get_network_features(
        scoping_customer=None,
        filters=None,
    ):
    """
    Get a list of features.
    These might be filtered by network service.

    :param filters: A dict of filters.
    """
    # In case the network service is passed as an object,
    # we use the filter pk. TODO: patch filtering fields to
    # accept objects.
    if filters:
        network_service = filters.get("network_service")
        if network_service and isinstance(network_service, NetworkService):
            filters["network_service"] = network_service.pk

    filtered = NetworkFeatureFilter(filters)

    return filtered.qs


def get_network_feature(
        scoping_customer=None,
        network_feature=None,
    ) -> NetworkFeature:
    """
    Retrieve a single feature by it's primary key

    :param feature: The pk of the feature
    """
    # Lookup strategies
    if network_feature and isinstance(network_feature, NetworkFeature):
        return network_feature

    return NetworkFeature.objects.get(pk=network_feature)


def assert_network_feature_available(
        network_feature=None,
        network_service=None,
        scoping_customer=None,
    ):
    """
    Check for the availability of a feature in a given network service.

    :raises NetworkFeatureNotAvailable: In case the feature is not available

    :param scoping_customer: The managing customer for interface homogeny
    :param network_feature: The network feature
    :param network_service: The service to check for feature availability
    """
    if not network_feature in network_service.network_features.all():
        raise NetworkFeatureNotAvailable(network_feature, network_service)

