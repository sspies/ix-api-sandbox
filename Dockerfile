
#
# Dockerfile for Development
#

FROM python:3.6
ENV PYTHONUNBUFFERED 1

RUN apt-get update && \
  apt-get install --no-install-recommends -y \
  inetutils-ping \
  netcat && \
  rm -rf /var/lib/apt/lists/*

# Setup project
RUN mkdir -p /code
COPY . /code

WORKDIR /code
RUN pip3 install -r requirements/development.txt

