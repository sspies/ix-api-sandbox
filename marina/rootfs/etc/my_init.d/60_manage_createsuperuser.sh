#!/usr/bin/env bash

if [[ ${JEA_SBX_SU_RESET} -eq 1 ]]; then
/sbin/setuser app python3 /home/app/sandbox/src/manage.py shell -c "from jea.auth.models import User; user = User.objects.create_user('${JEA_SBX_SU_USERNAME}', '${JEA_SBX_SU_EMAIL}', '${JEA_SBX_SU_PASSWORD}'); user.is_superuser = True; user.is_staff = True; user.save();" | tee /home/app/reset-logs/su-$(date +%d%m%y_%H%M%S).txt
fi
